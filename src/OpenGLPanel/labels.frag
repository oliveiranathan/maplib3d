/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
varying vec4 vertColor;
varying vec4 vertPos;
varying vec3 N;
varying vec3 v;

void main() {
	vec3 light_position = vec3(0.0, 500.0, -600.0);
	vec4 light_color_diffuse = vec4(1.0, 1.0, 1.0, 1.0);
	vec4 light_color_ambient = vec4(1.0, 1.0, 1.0, 1.0);
	vec4 light_color_specular = vec4(0.5, 0.5, 0.5, 1.0);
	vec3 light_direction = vec3(1000.0, 1000.0, 0.0);
	float shininess = 0.5;
	vec3 L = normalize(light_position - v);
	vec3 E = vec3(150.0, 150.0, -50.0);
	vec3 R = normalize(-reflect(L, -N));
	vec4 Iamb = light_color_ambient * 0.05;
	vec4 Idiff = light_color_diffuse * max(dot(N,L), 0.0);
	Idiff = clamp(Idiff, 0.0, 1.0) * 0.3;
	vec4 Ispec = light_color_specular * pow(max(dot(R, E), 0.0),0.3 * shininess);
	Ispec = clamp(Ispec, 0.0, 1.0) * 0.1;
	gl_FragColor = clamp(vec4(vertColor.rgb, 1.0) + Iamb + Idiff + Ispec, 0.0, 1.0);
}
