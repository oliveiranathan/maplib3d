/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
varying vec4 vertPos;
uniform float scale;
uniform float max;
uniform sampler2D mask;
uniform vec2 size;
uniform vec2 prop;
uniform int redBand;

vec3 hsb2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
	vec4 value = texture2D(mask, vec2(vertPos.x / size.x * prop.s, (vertPos.y / size.y) * prop.t));
	if (value.r > 0.5) {
		int band = int(-vertPos.z / scale + 0.5);
		if (redBand != band) {
			if (band == 0) {
				gl_FragColor = vec4(1.0, 1.0, 1.0, 0.8);
			} else {
				float H = 5.0 / 9.0 + float(band) / (9.0 * max);
				float S = 0.5 + 0.5 * float(band) / max;
				float B = 1.0 - 0.5 * float(band) / max;
				gl_FragColor = vec4(hsb2rgb(vec3(H, S, B)), 0.8);
			}
		} else {
			gl_FragColor = vec4(1.0, 0.0, 0.0, 0.8);
		}
	} else {
		gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
	}
}