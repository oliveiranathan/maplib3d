/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
package OpenGLPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.TextureImpl;

public class Labels {

	private static int vboID = 0;
	private static final float[] vertices = {0.0f, -20.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // <editor-fold defaultstate="collapsed" desc="x,y,z, NDx,NDy,NDz, NSx,NSy,NSz ...">
		20.0f, -5.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		20.0f, -20.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		19.960535f, -5.0f, 1.2558105f, 0.0f, 1.0f, 0.0f, 0.9980267f, 0.0f, 0.06279053f,
		19.960535f, -20.0f, 1.2558105f, 0.0f, -1.0f, 0.0f, 0.9980267f, 0.0f, 0.06279053f,
		19.842295f, -5.0f, 2.5066648f, 0.0f, 1.0f, 0.0f, 0.9921147f, 0.0f, 0.12533323f,
		19.842295f, -20.0f, 2.5066648f, 0.0f, -1.0f, 0.0f, 0.9921147f, 0.0f, 0.12533323f,
		19.645744f, -5.0f, 3.7476265f, 0.0f, 1.0f, 0.0f, 0.9822872f, 0.0f, 0.18738133f,
		19.645744f, -20.0f, 3.7476265f, 0.0f, -1.0f, 0.0f, 0.9822872f, 0.0f, 0.18738133f,
		19.371664f, -5.0f, 4.973798f, 0.0f, 1.0f, 0.0f, 0.9685831f, 0.0f, 0.24868986f,
		19.371664f, -20.0f, 4.973798f, 0.0f, -1.0f, 0.0f, 0.9685831f, 0.0f, 0.24868986f,
		19.02113f, -5.0f, 6.1803403f, 0.0f, 1.0f, 0.0f, 0.9510565f, 0.0f, 0.309017f,
		19.02113f, -20.0f, 6.1803403f, 0.0f, -1.0f, 0.0f, 0.9510565f, 0.0f, 0.309017f,
		18.59553f, -5.0f, 7.362491f, 0.0f, 1.0f, 0.0f, 0.9297765f, 0.0f, 0.36812454f,
		18.59553f, -20.0f, 7.362491f, 0.0f, -1.0f, 0.0f, 0.9297765f, 0.0f, 0.36812454f,
		18.09654f, -5.0f, 8.515586f, 0.0f, 1.0f, 0.0f, 0.904827f, 0.0f, 0.42577928f,
		18.09654f, -20.0f, 8.515586f, 0.0f, -1.0f, 0.0f, 0.904827f, 0.0f, 0.42577928f,
		17.526133f, -5.0f, 9.635074f, 0.0f, 1.0f, 0.0f, 0.87630665f, 0.0f, 0.48175368f,
		17.526133f, -20.0f, 9.635074f, 0.0f, -1.0f, 0.0f, 0.87630665f, 0.0f, 0.48175368f,
		16.886559f, -5.0f, 10.7165365f, 0.0f, 1.0f, 0.0f, 0.8443279f, 0.0f, 0.5358268f,
		16.886559f, -20.0f, 10.7165365f, 0.0f, -1.0f, 0.0f, 0.8443279f, 0.0f, 0.5358268f,
		16.18034f, -5.0f, 11.755706f, 0.0f, 1.0f, 0.0f, 0.809017f, 0.0f, 0.5877853f,
		16.18034f, -20.0f, 11.755706f, 0.0f, -1.0f, 0.0f, 0.809017f, 0.0f, 0.5877853f,
		15.410264f, -5.0f, 12.748482f, 0.0f, 1.0f, 0.0f, 0.7705132f, 0.0f, 0.6374241f,
		15.410264f, -20.0f, 12.748482f, 0.0f, -1.0f, 0.0f, 0.7705132f, 0.0f, 0.6374241f,
		14.5793705f, -5.0f, 13.690944f, 0.0f, 1.0f, 0.0f, 0.7289685f, 0.0f, 0.6845472f,
		14.5793705f, -20.0f, 13.690944f, 0.0f, -1.0f, 0.0f, 0.7289685f, 0.0f, 0.6845472f,
		13.69094f, -5.0f, 14.579374f, 0.0f, 1.0f, 0.0f, 0.684547f, 0.0f, 0.72896874f,
		13.69094f, -20.0f, 14.579374f, 0.0f, -1.0f, 0.0f, 0.684547f, 0.0f, 0.72896874f,
		12.748477f, -5.0f, 15.410267f, 0.0f, 1.0f, 0.0f, 0.6374239f, 0.0f, 0.77051336f,
		12.748477f, -20.0f, 15.410267f, 0.0f, -1.0f, 0.0f, 0.6374239f, 0.0f, 0.77051336f,
		11.755702f, -5.0f, 16.180342f, 0.0f, 1.0f, 0.0f, 0.5877851f, 0.0f, 0.80901706f,
		11.755702f, -20.0f, 16.180342f, 0.0f, -1.0f, 0.0f, 0.5877851f, 0.0f, 0.80901706f,
		10.716533f, -5.0f, 16.88656f, 0.0f, 1.0f, 0.0f, 0.5358266f, 0.0f, 0.84432805f,
		10.716533f, -20.0f, 16.88656f, 0.0f, -1.0f, 0.0f, 0.5358266f, 0.0f, 0.84432805f,
		9.63507f, -5.0f, 17.526136f, 0.0f, 1.0f, 0.0f, 0.4817535f, 0.0f, 0.87630683f,
		9.63507f, -20.0f, 17.526136f, 0.0f, -1.0f, 0.0f, 0.4817535f, 0.0f, 0.87630683f,
		8.515582f, -5.0f, 18.096542f, 0.0f, 1.0f, 0.0f, 0.4257791f, 0.0f, 0.9048271f,
		8.515582f, -20.0f, 18.096542f, 0.0f, -1.0f, 0.0f, 0.4257791f, 0.0f, 0.9048271f,
		7.3624864f, -5.0f, 18.595531f, 0.0f, 1.0f, 0.0f, 0.3681243f, 0.0f, 0.92977655f,
		7.3624864f, -20.0f, 18.595531f, 0.0f, -1.0f, 0.0f, 0.3681243f, 0.0f, 0.92977655f,
		6.1803346f, -5.0f, 19.021132f, 0.0f, 1.0f, 0.0f, 0.30901673f, 0.0f, 0.9510566f,
		6.1803346f, -20.0f, 19.021132f, 0.0f, -1.0f, 0.0f, 0.30901673f, 0.0f, 0.9510566f,
		4.973792f, -5.0f, 19.371664f, 0.0f, 1.0f, 0.0f, 0.2486896f, 0.0f, 0.9685832f,
		4.973792f, -20.0f, 19.371664f, 0.0f, -1.0f, 0.0f, 0.2486896f, 0.0f, 0.9685832f,
		3.7476199f, -5.0f, 19.645746f, 0.0f, 1.0f, 0.0f, 0.187381f, 0.0f, 0.9822873f,
		3.7476199f, -20.0f, 19.645746f, 0.0f, -1.0f, 0.0f, 0.187381f, 0.0f, 0.9822873f,
		2.5066578f, -5.0f, 19.842295f, 0.0f, 1.0f, 0.0f, 0.12533289f, 0.0f, 0.9921147f,
		2.5066578f, -20.0f, 19.842295f, 0.0f, -1.0f, 0.0f, 0.12533289f, 0.0f, 0.9921147f,
		1.2558029f, -5.0f, 19.960535f, 0.0f, 1.0f, 0.0f, 0.06279014f, 0.0f, 0.9980267f,
		1.2558029f, -20.0f, 19.960535f, 0.0f, -1.0f, 0.0f, 0.06279014f, 0.0f, 0.9980267f,
		-8.0267855E-6f, -5.0f, 20.0f, 0.0f, 1.0f, 0.0f, -4.0133926E-7f, 0.0f, 1.0f,
		-8.0267855E-6f, -20.0f, 20.0f, 0.0f, -1.0f, 0.0f, -4.0133926E-7f, 0.0f, 1.0f,
		-1.255819f, -5.0f, 19.960533f, 0.0f, 1.0f, 0.0f, -0.062790945f, 0.0f, 0.99802667f,
		-1.255819f, -20.0f, 19.960533f, 0.0f, -1.0f, 0.0f, -0.062790945f, 0.0f, 0.99802667f,
		-2.5066736f, -5.0f, 19.842293f, 0.0f, 1.0f, 0.0f, -0.12533368f, 0.0f, 0.99211466f,
		-2.5066736f, -20.0f, 19.842293f, 0.0f, -1.0f, 0.0f, -0.12533368f, 0.0f, 0.99211466f,
		-3.7476356f, -5.0f, 19.645742f, 0.0f, 1.0f, 0.0f, -0.18738177f, 0.0f, 0.9822871f,
		-3.7476356f, -20.0f, 19.645742f, 0.0f, -1.0f, 0.0f, -0.18738177f, 0.0f, 0.9822871f,
		-4.9738073f, -5.0f, 19.37166f, 0.0f, 1.0f, 0.0f, -0.24869037f, 0.0f, 0.968583f,
		-4.9738073f, -20.0f, 19.37166f, 0.0f, -1.0f, 0.0f, -0.24869037f, 0.0f, 0.968583f,
		-6.18035f, -5.0f, 19.021128f, 0.0f, 1.0f, 0.0f, -0.30901748f, 0.0f, 0.95105636f,
		-6.18035f, -20.0f, 19.021128f, 0.0f, -1.0f, 0.0f, -0.30901748f, 0.0f, 0.95105636f,
		-7.362501f, -5.0f, 18.595526f, 0.0f, 1.0f, 0.0f, -0.36812505f, 0.0f, 0.9297763f,
		-7.362501f, -20.0f, 18.595526f, 0.0f, -1.0f, 0.0f, -0.36812505f, 0.0f, 0.9297763f,
		-8.515596f, -5.0f, 18.096537f, 0.0f, 1.0f, 0.0f, -0.42577982f, 0.0f, 0.9048268f,
		-8.515596f, -20.0f, 18.096537f, 0.0f, -1.0f, 0.0f, -0.42577982f, 0.0f, 0.9048268f,
		-9.635084f, -5.0f, 17.526127f, 0.0f, 1.0f, 0.0f, -0.4817542f, 0.0f, 0.87630635f,
		-9.635084f, -20.0f, 17.526127f, 0.0f, -1.0f, 0.0f, -0.4817542f, 0.0f, 0.87630635f,
		-10.716547f, -5.0f, 16.88655f, 0.0f, 1.0f, 0.0f, -0.53582734f, 0.0f, 0.84432757f,
		-10.716547f, -20.0f, 16.88655f, 0.0f, -1.0f, 0.0f, -0.53582734f, 0.0f, 0.84432757f,
		-11.755715f, -5.0f, 16.180332f, 0.0f, 1.0f, 0.0f, -0.5877858f, 0.0f, 0.8090166f,
		-11.755715f, -20.0f, 16.180332f, 0.0f, -1.0f, 0.0f, -0.5877858f, 0.0f, 0.8090166f,
		-12.74849f, -5.0f, 15.410256f, 0.0f, 1.0f, 0.0f, -0.6374245f, 0.0f, 0.7705128f,
		-12.74849f, -20.0f, 15.410256f, 0.0f, -1.0f, 0.0f, -0.6374245f, 0.0f, 0.7705128f,
		-13.690952f, -5.0f, 14.579363f, 0.0f, 1.0f, 0.0f, -0.6845476f, 0.0f, 0.72896814f,
		-13.690952f, -20.0f, 14.579363f, 0.0f, -1.0f, 0.0f, -0.6845476f, 0.0f, 0.72896814f,
		-14.579383f, -5.0f, 13.690931f, 0.0f, 1.0f, 0.0f, -0.72896916f, 0.0f, 0.6845466f,
		-14.579383f, -20.0f, 13.690931f, 0.0f, -1.0f, 0.0f, -0.72896916f, 0.0f, 0.6845466f,
		-15.4102745f, -5.0f, 12.748468f, 0.0f, 1.0f, 0.0f, -0.7705137f, 0.0f, 0.6374234f,
		-15.4102745f, -20.0f, 12.748468f, 0.0f, -1.0f, 0.0f, -0.7705137f, 0.0f, 0.6374234f,
		-16.18035f, -5.0f, 11.7556925f, 0.0f, 1.0f, 0.0f, -0.8090175f, 0.0f, 0.58778465f,
		-16.18035f, -20.0f, 11.7556925f, 0.0f, -1.0f, 0.0f, -0.8090175f, 0.0f, 0.58778465f,
		-16.886568f, -5.0f, 10.716522f, 0.0f, 1.0f, 0.0f, -0.8443284f, 0.0f, 0.5358261f,
		-16.886568f, -20.0f, 10.716522f, 0.0f, -1.0f, 0.0f, -0.8443284f, 0.0f, 0.5358261f,
		-17.526142f, -5.0f, 9.635058f, 0.0f, 1.0f, 0.0f, -0.87630713f, 0.0f, 0.48175293f,
		-17.526142f, -20.0f, 9.635058f, 0.0f, -1.0f, 0.0f, -0.87630713f, 0.0f, 0.48175293f,
		-18.096548f, -5.0f, 8.515571f, 0.0f, 1.0f, 0.0f, -0.9048274f, 0.0f, 0.42577854f,
		-18.096548f, -20.0f, 8.515571f, 0.0f, -1.0f, 0.0f, -0.9048274f, 0.0f, 0.42577854f,
		-18.595537f, -5.0f, 7.3624744f, 0.0f, 1.0f, 0.0f, -0.92977685f, 0.0f, 0.3681237f,
		-18.595537f, -20.0f, 7.3624744f, 0.0f, -1.0f, 0.0f, -0.92977685f, 0.0f, 0.3681237f,
		-19.021135f, -5.0f, 6.1803226f, 0.0f, 1.0f, 0.0f, -0.9510568f, 0.0f, 0.30901614f,
		-19.021135f, -20.0f, 6.1803226f, 0.0f, -1.0f, 0.0f, -0.9510568f, 0.0f, 0.30901614f,
		-19.371668f, -5.0f, 4.9737797f, 0.0f, 1.0f, 0.0f, -0.9685834f, 0.0f, 0.24868898f,
		-19.371668f, -20.0f, 4.9737797f, 0.0f, -1.0f, 0.0f, -0.9685834f, 0.0f, 0.24868898f,
		-19.645748f, -5.0f, 3.7476072f, 0.0f, 1.0f, 0.0f, -0.9822874f, 0.0f, 0.18738036f,
		-19.645748f, -20.0f, 3.7476072f, 0.0f, -1.0f, 0.0f, -0.9822874f, 0.0f, 0.18738036f,
		-19.842297f, -5.0f, 2.506645f, 0.0f, 1.0f, 0.0f, -0.99211484f, 0.0f, 0.12533225f,
		-19.842297f, -20.0f, 2.506645f, 0.0f, -1.0f, 0.0f, -0.99211484f, 0.0f, 0.12533225f,
		-19.960535f, -5.0f, 1.2557901f, 0.0f, 1.0f, 0.0f, -0.9980267f, 0.0f, 0.06278951f,
		-19.960535f, -20.0f, 1.2557901f, 0.0f, -1.0f, 0.0f, -0.9980267f, 0.0f, 0.06278951f,
		-20.0f, -5.0f, -2.0821943E-5f, 0.0f, 1.0f, 0.0f, -1.0f, 0.0f, -1.0410971E-6f,
		-20.0f, -20.0f, -2.0821943E-5f, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, -1.0410971E-6f,
		-19.960533f, -5.0f, -1.2558317f, 0.0f, 1.0f, 0.0f, -0.99802667f, 0.0f, -0.062791586f,
		-19.960533f, -20.0f, -1.2558317f, 0.0f, -1.0f, 0.0f, -0.99802667f, 0.0f, -0.062791586f,
		-19.84229f, -5.0f, -2.5066864f, 0.0f, 1.0f, 0.0f, -0.99211454f, 0.0f, -0.12533432f,
		-19.84229f, -20.0f, -2.5066864f, 0.0f, -1.0f, 0.0f, -0.99211454f, 0.0f, -0.12533432f,
		-19.64574f, -5.0f, -3.7476482f, 0.0f, 1.0f, 0.0f, -0.98228705f, 0.0f, -0.18738241f,
		-19.64574f, -20.0f, -3.7476482f, 0.0f, -1.0f, 0.0f, -0.98228705f, 0.0f, -0.18738241f,
		-19.371658f, -5.0f, -4.9738197f, 0.0f, 1.0f, 0.0f, -0.9685829f, 0.0f, -0.248691f,
		-19.371658f, -20.0f, -4.9738197f, 0.0f, -1.0f, 0.0f, -0.9685829f, 0.0f, -0.248691f,
		-19.021124f, -5.0f, -6.180362f, 0.0f, 1.0f, 0.0f, -0.9510562f, 0.0f, -0.3090181f,
		-19.021124f, -20.0f, -6.180362f, 0.0f, -1.0f, 0.0f, -0.9510562f, 0.0f, -0.3090181f,
		-18.59552f, -5.0f, -7.362513f, 0.0f, 1.0f, 0.0f, -0.929776f, 0.0f, -0.36812565f,
		-18.59552f, -20.0f, -7.362513f, 0.0f, -1.0f, 0.0f, -0.929776f, 0.0f, -0.36812565f,
		-18.09653f, -5.0f, -8.515608f, 0.0f, 1.0f, 0.0f, -0.9048265f, 0.0f, -0.4257804f,
		-18.09653f, -20.0f, -8.515608f, 0.0f, -1.0f, 0.0f, -0.9048265f, 0.0f, -0.4257804f,
		-17.526121f, -5.0f, -9.635096f, 0.0f, 1.0f, 0.0f, -0.87630606f, 0.0f, -0.48175478f,
		-17.526121f, -20.0f, -9.635096f, 0.0f, -1.0f, 0.0f, -0.87630606f, 0.0f, -0.48175478f,
		-16.886545f, -5.0f, -10.7165575f, 0.0f, 1.0f, 0.0f, -0.8443273f, 0.0f, -0.5358279f,
		-16.886545f, -20.0f, -10.7165575f, 0.0f, -1.0f, 0.0f, -0.8443273f, 0.0f, -0.5358279f,
		-16.180325f, -5.0f, -11.755726f, 0.0f, 1.0f, 0.0f, -0.8090162f, 0.0f, -0.5877863f,
		-16.180325f, -20.0f, -11.755726f, 0.0f, -1.0f, 0.0f, -0.8090162f, 0.0f, -0.5877863f,
		-15.410248f, -5.0f, -12.7485f, 0.0f, 1.0f, 0.0f, -0.7705124f, 0.0f, -0.637425f,
		-15.410248f, -20.0f, -12.7485f, 0.0f, -1.0f, 0.0f, -0.7705124f, 0.0f, -0.637425f,
		-14.579354f, -5.0f, -13.690962f, 0.0f, 1.0f, 0.0f, -0.7289677f, 0.0f, -0.6845481f,
		-14.579354f, -20.0f, -13.690962f, 0.0f, -1.0f, 0.0f, -0.7289677f, 0.0f, -0.6845481f,
		-13.690922f, -5.0f, -14.5793915f, 0.0f, 1.0f, 0.0f, -0.6845461f, 0.0f, -0.7289696f,
		-13.690922f, -20.0f, -14.5793915f, 0.0f, -1.0f, 0.0f, -0.6845461f, 0.0f, -0.7289696f,
		-12.748462f, -5.0f, -15.410279f, 0.0f, 1.0f, 0.0f, -0.6374231f, 0.0f, -0.77051395f,
		-12.748462f, -20.0f, -15.410279f, 0.0f, -1.0f, 0.0f, -0.6374231f, 0.0f, -0.77051395f,
		-11.755686f, -5.0f, -16.180353f, 0.0f, 1.0f, 0.0f, -0.5877843f, 0.0f, -0.80901766f,
		-11.755686f, -20.0f, -16.180353f, 0.0f, -1.0f, 0.0f, -0.5877843f, 0.0f, -0.80901766f,
		-10.716516f, -5.0f, -16.886572f, 0.0f, 1.0f, 0.0f, -0.5358258f, 0.0f, -0.8443286f,
		-10.716516f, -20.0f, -16.886572f, 0.0f, -1.0f, 0.0f, -0.5358258f, 0.0f, -0.8443286f,
		-9.635052f, -5.0f, -17.526146f, 0.0f, 1.0f, 0.0f, -0.48175257f, 0.0f, -0.8763073f,
		-9.635052f, -20.0f, -17.526146f, 0.0f, -1.0f, 0.0f, -0.48175257f, 0.0f, -0.8763073f,
		-8.515563f, -5.0f, -18.096552f, 0.0f, 1.0f, 0.0f, -0.42577815f, 0.0f, -0.9048276f,
		-8.515563f, -20.0f, -18.096552f, 0.0f, -1.0f, 0.0f, -0.42577815f, 0.0f, -0.9048276f,
		-7.3624673f, -5.0f, -18.59554f, 0.0f, 1.0f, 0.0f, -0.36812335f, 0.0f, -0.92977697f,
		-7.3624673f, -20.0f, -18.59554f, 0.0f, -1.0f, 0.0f, -0.36812335f, 0.0f, -0.92977697f,
		-6.180315f, -5.0f, -19.02114f, 0.0f, 1.0f, 0.0f, -0.30901575f, 0.0f, -0.95105696f,
		-6.180315f, -20.0f, -19.02114f, 0.0f, -1.0f, 0.0f, -0.30901575f, 0.0f, -0.95105696f,
		-4.9737716f, -5.0f, -19.37167f, 0.0f, 1.0f, 0.0f, -0.24868858f, 0.0f, -0.96858346f,
		-4.9737716f, -20.0f, -19.37167f, 0.0f, -1.0f, 0.0f, -0.24868858f, 0.0f, -0.96858346f,
		-3.7475994f, -5.0f, -19.64575f, 0.0f, 1.0f, 0.0f, -0.18737997f, 0.0f, -0.9822875f,
		-3.7475994f, -20.0f, -19.64575f, 0.0f, -1.0f, 0.0f, -0.18737997f, 0.0f, -0.9822875f,
		-2.506637f, -5.0f, -19.842297f, 0.0f, 1.0f, 0.0f, -0.12533185f, 0.0f, -0.99211484f,
		-2.506637f, -20.0f, -19.842297f, 0.0f, -1.0f, 0.0f, -0.12533185f, 0.0f, -0.99211484f,
		-1.2557821f, -5.0f, -19.960537f, 0.0f, 1.0f, 0.0f, -0.062789105f, 0.0f, -0.99802685f,
		-1.2557821f, -20.0f, -19.960537f, 0.0f, -1.0f, 0.0f, -0.062789105f, 0.0f, -0.99802685f,
		2.8848726E-5f, -5.0f, -20.0f, 0.0f, 1.0f, 0.0f, 1.4424363E-6f, 0.0f, -1.0f,
		2.8848726E-5f, -20.0f, -20.0f, 0.0f, -1.0f, 0.0f, 1.4424363E-6f, 0.0f, -1.0f,
		1.2558397f, -5.0f, -19.960533f, 0.0f, 1.0f, 0.0f, 0.06279199f, 0.0f, -0.99802667f,
		1.2558397f, -20.0f, -19.960533f, 0.0f, -1.0f, 0.0f, 0.06279199f, 0.0f, -0.99802667f,
		2.5066943f, -5.0f, -19.84229f, 0.0f, 1.0f, 0.0f, 0.12533471f, 0.0f, -0.99211454f,
		2.5066943f, -20.0f, -19.84229f, 0.0f, -1.0f, 0.0f, 0.12533471f, 0.0f, -0.99211454f,
		3.747656f, -5.0f, -19.645739f, 0.0f, 1.0f, 0.0f, 0.1873828f, 0.0f, -0.98228693f,
		3.747656f, -20.0f, -19.645739f, 0.0f, -1.0f, 0.0f, 0.1873828f, 0.0f, -0.98228693f,
		4.973828f, -5.0f, -19.371656f, 0.0f, 1.0f, 0.0f, 0.2486914f, 0.0f, -0.9685828f,
		4.973828f, -20.0f, -19.371656f, 0.0f, -1.0f, 0.0f, 0.2486914f, 0.0f, -0.9685828f,
		6.18037f, -5.0f, -19.02112f, 0.0f, 1.0f, 0.0f, 0.3090185f, 0.0f, -0.951056f,
		6.18037f, -20.0f, -19.02112f, 0.0f, -1.0f, 0.0f, 0.3090185f, 0.0f, -0.951056f,
		7.3625207f, -5.0f, -18.595518f, 0.0f, 1.0f, 0.0f, 0.36812603f, 0.0f, -0.9297759f,
		7.3625207f, -20.0f, -18.595518f, 0.0f, -1.0f, 0.0f, 0.36812603f, 0.0f, -0.9297759f,
		8.515615f, -5.0f, -18.096527f, 0.0f, 1.0f, 0.0f, 0.42578077f, 0.0f, -0.90482634f,
		8.515615f, -20.0f, -18.096527f, 0.0f, -1.0f, 0.0f, 0.42578077f, 0.0f, -0.90482634f,
		9.635102f, -5.0f, -17.526117f, 0.0f, 1.0f, 0.0f, 0.4817551f, 0.0f, -0.8763059f,
		9.635102f, -20.0f, -17.526117f, 0.0f, -1.0f, 0.0f, 0.4817551f, 0.0f, -0.8763059f,
		10.716564f, -5.0f, -16.886541f, 0.0f, 1.0f, 0.0f, 0.53582823f, 0.0f, -0.8443271f,
		10.716564f, -20.0f, -16.886541f, 0.0f, -1.0f, 0.0f, 0.53582823f, 0.0f, -0.8443271f,
		11.755733f, -5.0f, -16.18032f, 0.0f, 1.0f, 0.0f, 0.5877866f, 0.0f, -0.80901605f,
		11.755733f, -20.0f, -16.18032f, 0.0f, -1.0f, 0.0f, 0.5877866f, 0.0f, -0.80901605f,
		12.748507f, -5.0f, -15.410243f, 0.0f, 1.0f, 0.0f, 0.6374253f, 0.0f, -0.77051216f,
		12.748507f, -20.0f, -15.410243f, 0.0f, -1.0f, 0.0f, 0.6374253f, 0.0f, -0.77051216f,
		13.690968f, -5.0f, -14.579349f, 0.0f, 1.0f, 0.0f, 0.6845484f, 0.0f, -0.7289674f,
		13.690968f, -20.0f, -14.579349f, 0.0f, -1.0f, 0.0f, 0.6845484f, 0.0f, -0.7289674f,
		14.579397f, -5.0f, -13.690916f, 0.0f, 1.0f, 0.0f, 0.7289699f, 0.0f, -0.6845458f,
		14.579397f, -20.0f, -13.690916f, 0.0f, -1.0f, 0.0f, 0.7289699f, 0.0f, -0.6845458f,
		15.410288f, -5.0f, -12.748452f, 0.0f, 1.0f, 0.0f, 0.77051437f, 0.0f, -0.6374226f,
		15.410288f, -20.0f, -12.748452f, 0.0f, -1.0f, 0.0f, 0.77051437f, 0.0f, -0.6374226f,
		16.18036f, -5.0f, -11.755675f, 0.0f, 1.0f, 0.0f, 0.809018f, 0.0f, -0.58778375f,
		16.18036f, -20.0f, -11.755675f, 0.0f, -1.0f, 0.0f, 0.809018f, 0.0f, -0.58778375f,
		16.886578f, -5.0f, -10.716505f, 0.0f, 1.0f, 0.0f, 0.8443289f, 0.0f, -0.53582525f,
		16.886578f, -20.0f, -10.716505f, 0.0f, -1.0f, 0.0f, 0.8443289f, 0.0f, -0.53582525f,
		17.526152f, -5.0f, -9.63504f, 0.0f, 1.0f, 0.0f, 0.8763076f, 0.0f, -0.481752f,
		17.526152f, -20.0f, -9.63504f, 0.0f, -1.0f, 0.0f, 0.8763076f, 0.0f, -0.481752f,
		18.096558f, -5.0f, -8.515552f, 0.0f, 1.0f, 0.0f, 0.9048279f, 0.0f, -0.42577758f,
		18.096558f, -20.0f, -8.515552f, 0.0f, -1.0f, 0.0f, 0.9048279f, 0.0f, -0.42577758f,
		18.595545f, -5.0f, -7.3624554f, 0.0f, 1.0f, 0.0f, 0.92977726f, 0.0f, -0.36812276f,
		18.595545f, -20.0f, -7.3624554f, 0.0f, -1.0f, 0.0f, 0.92977726f, 0.0f, -0.36812276f,
		19.021143f, -5.0f, -6.1803026f, 0.0f, 1.0f, 0.0f, 0.95105714f, 0.0f, -0.30901513f,
		19.021143f, -20.0f, -6.1803026f, 0.0f, -1.0f, 0.0f, 0.95105714f, 0.0f, -0.30901513f,
		19.371674f, -5.0f, -4.973759f, 0.0f, 1.0f, 0.0f, 0.9685837f, 0.0f, -0.24868795f,
		19.371674f, -20.0f, -4.973759f, 0.0f, -1.0f, 0.0f, 0.9685837f, 0.0f, -0.24868795f,
		19.645752f, -5.0f, -3.747587f, 0.0f, 1.0f, 0.0f, 0.9822876f, 0.0f, -0.18737935f,
		19.645752f, -20.0f, -3.747587f, 0.0f, -1.0f, 0.0f, 0.9822876f, 0.0f, -0.18737935f,
		19.842299f, -5.0f, -2.5066245f, 0.0f, 1.0f, 0.0f, 0.9921149f, 0.0f, -0.12533122f,
		19.842299f, -20.0f, -2.5066245f, 0.0f, -1.0f, 0.0f, 0.9921149f, 0.0f, -0.12533122f,
		19.960537f, -5.0f, -1.2557694f, 0.0f, 1.0f, 0.0f, 0.99802685f, 0.0f, -0.06278847f,
		19.960537f, -20.0f, -1.2557694f, 0.0f, -1.0f, 0.0f, 0.99802685f, 0.0f, -0.06278847f,
		20.0f, -5.0f, 4.1643885E-5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 2.0821942E-6f,
		// </editor-fold>
		20.0f, -20.0f, 4.1643885E-5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 2.0821942E-6f};
	private static int vertShadID = 0;
	private static final String vertexShaderCode = OpenGLEngine.readFile(Pizza.class.getResourceAsStream("/OpenGLPanel/labels.vert"), Charset.defaultCharset());
	private static int fragShadID = 0;
	private static final String fragmentShaderCode = OpenGLEngine.readFile(Pizza.class.getResourceAsStream("/OpenGLPanel/labels.frag"), Charset.defaultCharset());
	private static int shader = 0;

	private static final float radius = 20;
	private static final float height = 15;

	private final String[] labels;
	private final Color[] colors;
	private final float[] histogram;
	private final int[] colorIndexes;
	private final Gradient gradient;
	private final int x, y, w, h;
	private TrueTypeFont font = null;

	public Labels(String[] labels, Color[] colors, Gradient g, int x, int y, int w, int h) {
		if (labels == null || colors == null || labels.length != colors.length) {
			throw new IllegalArgumentException("Labels and colors must have the same size");
		}
		this.labels = labels;
		this.colors = colors;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h - 32;
		histogram = null;
		colorIndexes = null;
		gradient = g;
	}

	public Labels(String[] labels, Color[] colors, float[] histogram, int[] colorIndexes, Gradient g, int x, int y, int w, int h) {
		if (labels == null || colors == null || histogram == null || labels.length != colors.length || labels.length != histogram.length) {
			throw new IllegalArgumentException("Labels and colors must have the same size");
		}
		this.labels = labels;
		this.colors = colors;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h - 32;
		this.histogram = histogram;
		this.colorIndexes = colorIndexes;
		gradient = g;
	}

	public Labels(String[] labels, Color[] colors, int[] count, int[] colorIndexes, Gradient g, int x, int y, int w, int h) {
		if (labels == null || colors == null || count == null || labels.length != colors.length || labels.length != count.length) {
			throw new IllegalArgumentException("Labels and colors must have the same size");
		}
		this.labels = labels;
		this.colors = colors;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		float[] hist = new float[count.length];
		float total = 0;
		for (int i : count) {
			total += i;
		}
		for (int i = 0; i < count.length; i++) {
			hist[i] = (float) count[i] / total;
		}
		histogram = hist;
		this.colorIndexes = colorIndexes;
		gradient = g;
	}

	protected void draw() throws Exception {
		init();
		GL11.glColor3f(0, 0, 0);
		if (histogram != null) {
			GL11.glBegin(GL11.GL_LINE_STRIP);
			GL11.glVertex3f(x + 45, h - 205, 0);
			GL11.glVertex3f(x + 45, h - 5, 0);
			GL11.glVertex3f(x + 295, h - 5, 0);
			GL11.glVertex3f(x + 295, h - 205, 0);
			GL11.glVertex3f(x + 45, h - 205, 0);
			GL11.glEnd();
		}
		for (int i = 0; i < labels.length; i++) {
			if (gradient == null || gradient.getRedBand() != i) {
				GL11.glColor3ub((byte) colors[i].getRed(), (byte) colors[i].getGreen(), (byte) colors[i].getBlue());
			} else {
				GL11.glColor3f(1, 0, 0);
			}
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2d(x + 5, 5 + i * 25);
			GL11.glVertex2d(x + 5 + 20, 5 + i * 25);
			GL11.glVertex2d(x + 5 + 20, 5 + i * 25 + 20);
			GL11.glVertex2d(x + 5, 5 + i * 25 + 20);
			GL11.glEnd();
		}
		if (histogram != null) {
			ARBShaderObjects.glUseProgramObjectARB(shader);
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
			float dx = 250.f / (float) histogram.length;
			float scaleXZ = dx / radius / 3;
			float scaleY = 200.f / height;
			for (int i = 0; i < histogram.length; i++) {
				GL11.glColor3ub((byte) colors[colorIndexes[i]].getRed(), (byte) colors[colorIndexes[i]].getGreen(), (byte) colors[colorIndexes[i]].getBlue());
				GL11.glPushMatrix();
				GL11.glTranslatef(x + 45 + radius * 1.5f * scaleXZ + dx * i, h + scaleY * histogram[i] * 5, 0);
				GL11.glScalef(scaleXZ, scaleY * histogram[i], scaleXZ);
				GL11.glTranslatef(0, -5 / (scaleY * histogram[i]), 0);
				GL11.glVertexPointer(3, GL11.GL_FLOAT, 3 * 3 * 4 * 2, 0);// disc
				GL11.glNormalPointer(GL11.GL_FLOAT, 3 * 3 * 4 * 2, 3 * 4);
				GL11.glDrawArrays(GL11.GL_TRIANGLE_FAN, 0, 102);
				GL11.glVertexPointer(3, GL11.GL_FLOAT, 3 * 3 * 4, 3 * 3 * 4);// strip
				GL11.glNormalPointer(GL11.GL_FLOAT, 3 * 3 * 4, 3 * 3 * 4 + 3 * 4 * 2);
				GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 202);
				GL11.glPopMatrix();
			}
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
			ARBShaderObjects.glUseProgramObjectARB(0);
		}
		TextureImpl.bindNone();
		for (int i = 0; i < labels.length; i++) {
			font.drawString(x + 30, 5 + i * 25, labels[i], org.newdawn.slick.Color.black);
		}
		if (histogram != null) {
			GL11.glColor3f(0, 0, 0);
			GL11.glBegin(GL11.GL_LINES);
			GL11.glVertex3f(x + 30, 0, 0);
			GL11.glVertex3f(x + 30, h, 0);
			GL11.glEnd();
			font.drawString(x + 5, h - 215, "100 %", org.newdawn.slick.Color.black);
			font.drawString(x + 5, h - 20, "  0 %", org.newdawn.slick.Color.black);
		}
	}

	private void init() throws Exception {
		if (font == null) {
			try {
				font = new TrueTypeFont(Font.createFont(Font.TRUETYPE_FONT, Labels.class.getResourceAsStream("/Resources/font.ttf")).deriveFont(14.f), true);
			} catch (FontFormatException | IOException ex) {
				Logger.getLogger(Labels.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		if (vboID == 0) {
			vboID = GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
			FloatBuffer fb = BufferUtils.createFloatBuffer(vertices.length);
			fb.put(vertices);
			fb.rewind();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
		}
		if (vertShadID == 0) {
			vertShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
			if (vertShadID == 0) {
				throw new Exception("Cannot create the vertex shader.");
			}
			ARBShaderObjects.glShaderSourceARB(vertShadID, vertexShaderCode);
			ARBShaderObjects.glCompileShaderARB(vertShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(vertShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(vertShadID);
				vertShadID = 0;
				throw new Exception("Cannot compile vertex shader.");
			}
		}
		if (fragShadID == 0) {
			fragShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
			if (fragShadID == 0) {
				throw new Exception("Cannot create the fragment shader.");
			}
			ARBShaderObjects.glShaderSourceARB(fragShadID, fragmentShaderCode);
			ARBShaderObjects.glCompileShaderARB(fragShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(fragShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(fragShadID);
				fragShadID = 0;
				throw new Exception("Cannot compile fragment shader.");
			}
		}
		if (shader == 0) {
			shader = ARBShaderObjects.glCreateProgramObjectARB();
			if (shader == 0) {
				throw new Exception("Cannot create the shader program.");
			}
			ARBShaderObjects.glAttachObjectARB(shader, vertShadID);
			ARBShaderObjects.glAttachObjectARB(shader, fragShadID);
			ARBShaderObjects.glLinkProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error linking the shader.");
			}
			ARBShaderObjects.glValidateProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error validating the shader.");
			}
		}
	}

	protected void destroy() {
		if (font != null) {
			font = null;
		}
		if (vboID != 0) {
			GL15.glDeleteBuffers(vboID);
			vboID = 0;
		}
		if (shader != 0) {
			ARBShaderObjects.glDeleteObjectARB(shader);
			shader = 0;
		}
		if (vertShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(vertShadID);
			vertShadID = 0;
		}
		if (fragShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(fragShadID);
			fragShadID = 0;
		}
	}
}
