/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
package OpenGLPanel;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

/**
 *
 * @author Nathan Oliveira
 */
public class Pizza {

	private static int vboID = 0;
	private static final float[] vertices = {0.0f, 0.0f, -20.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // <editor-fold defaultstate="collapsed" desc="x,y,z, NDx,NDy,NDz, NSx,NSy,NSz ...">
		20.0f, 0.0f, -5.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
		20.0f, 0.0f, -20.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f,
		19.960535f, 1.2558105f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9980267f, 0.06279053f, 0.0f,
		19.960535f, 1.2558105f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9980267f, 0.06279053f, 0.0f,
		19.842295f, 2.5066648f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9921147f, 0.12533323f, 0.0f,
		19.842295f, 2.5066648f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9921147f, 0.12533323f, 0.0f,
		19.645744f, 3.7476265f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9822872f, 0.18738133f, 0.0f,
		19.645744f, 3.7476265f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9822872f, 0.18738133f, 0.0f,
		19.371664f, 4.973798f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9685831f, 0.24868986f, 0.0f,
		19.371664f, 4.973798f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9685831f, 0.24868986f, 0.0f,
		19.02113f, 6.1803403f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9510565f, 0.309017f, 0.0f,
		19.02113f, 6.1803403f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9510565f, 0.309017f, 0.0f,
		18.59553f, 7.362491f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9297765f, 0.36812454f, 0.0f,
		18.59553f, 7.362491f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9297765f, 0.36812454f, 0.0f,
		18.09654f, 8.515586f, -5.0f, 0.0f, 0.0f, 1.0f, 0.904827f, 0.42577928f, 0.0f,
		18.09654f, 8.515586f, -20.0f, 0.0f, 0.0f, -1.0f, 0.904827f, 0.42577928f, 0.0f,
		17.526133f, 9.635074f, -5.0f, 0.0f, 0.0f, 1.0f, 0.87630665f, 0.48175368f, 0.0f,
		17.526133f, 9.635074f, -20.0f, 0.0f, 0.0f, -1.0f, 0.87630665f, 0.48175368f, 0.0f,
		16.886559f, 10.7165365f, -5.0f, 0.0f, 0.0f, 1.0f, 0.8443279f, 0.5358268f, 0.0f,
		16.886559f, 10.7165365f, -20.0f, 0.0f, 0.0f, -1.0f, 0.8443279f, 0.5358268f, 0.0f,
		16.18034f, 11.755706f, -5.0f, 0.0f, 0.0f, 1.0f, 0.809017f, 0.5877853f, 0.0f,
		16.18034f, 11.755706f, -20.0f, 0.0f, 0.0f, -1.0f, 0.809017f, 0.5877853f, 0.0f,
		15.410264f, 12.748482f, -5.0f, 0.0f, 0.0f, 1.0f, 0.7705132f, 0.6374241f, 0.0f,
		15.410264f, 12.748482f, -20.0f, 0.0f, 0.0f, -1.0f, 0.7705132f, 0.6374241f, 0.0f,
		14.5793705f, 13.690944f, -5.0f, 0.0f, 0.0f, 1.0f, 0.7289685f, 0.6845472f, 0.0f,
		14.5793705f, 13.690944f, -20.0f, 0.0f, 0.0f, -1.0f, 0.7289685f, 0.6845472f, 0.0f,
		13.69094f, 14.579374f, -5.0f, 0.0f, 0.0f, 1.0f, 0.684547f, 0.72896874f, 0.0f,
		13.69094f, 14.579374f, -20.0f, 0.0f, 0.0f, -1.0f, 0.684547f, 0.72896874f, 0.0f,
		12.748477f, 15.410267f, -5.0f, 0.0f, 0.0f, 1.0f, 0.6374239f, 0.77051336f, 0.0f,
		12.748477f, 15.410267f, -20.0f, 0.0f, 0.0f, -1.0f, 0.6374239f, 0.77051336f, 0.0f,
		11.755702f, 16.180342f, -5.0f, 0.0f, 0.0f, 1.0f, 0.5877851f, 0.80901706f, 0.0f,
		11.755702f, 16.180342f, -20.0f, 0.0f, 0.0f, -1.0f, 0.5877851f, 0.80901706f, 0.0f,
		10.716533f, 16.88656f, -5.0f, 0.0f, 0.0f, 1.0f, 0.5358266f, 0.84432805f, 0.0f,
		10.716533f, 16.88656f, -20.0f, 0.0f, 0.0f, -1.0f, 0.5358266f, 0.84432805f, 0.0f,
		9.63507f, 17.526136f, -5.0f, 0.0f, 0.0f, 1.0f, 0.4817535f, 0.87630683f, 0.0f,
		9.63507f, 17.526136f, -20.0f, 0.0f, 0.0f, -1.0f, 0.4817535f, 0.87630683f, 0.0f,
		8.515582f, 18.096542f, -5.0f, 0.0f, 0.0f, 1.0f, 0.4257791f, 0.9048271f, 0.0f,
		8.515582f, 18.096542f, -20.0f, 0.0f, 0.0f, -1.0f, 0.4257791f, 0.9048271f, 0.0f,
		7.3624864f, 18.595531f, -5.0f, 0.0f, 0.0f, 1.0f, 0.3681243f, 0.92977655f, 0.0f,
		7.3624864f, 18.595531f, -20.0f, 0.0f, 0.0f, -1.0f, 0.3681243f, 0.92977655f, 0.0f,
		6.1803346f, 19.021132f, -5.0f, 0.0f, 0.0f, 1.0f, 0.30901673f, 0.9510566f, 0.0f,
		6.1803346f, 19.021132f, -20.0f, 0.0f, 0.0f, -1.0f, 0.30901673f, 0.9510566f, 0.0f,
		4.973792f, 19.371664f, -5.0f, 0.0f, 0.0f, 1.0f, 0.2486896f, 0.9685832f, 0.0f,
		4.973792f, 19.371664f, -20.0f, 0.0f, 0.0f, -1.0f, 0.2486896f, 0.9685832f, 0.0f,
		3.7476199f, 19.645746f, -5.0f, 0.0f, 0.0f, 1.0f, 0.187381f, 0.9822873f, 0.0f,
		3.7476199f, 19.645746f, -20.0f, 0.0f, 0.0f, -1.0f, 0.187381f, 0.9822873f, 0.0f,
		2.5066578f, 19.842295f, -5.0f, 0.0f, 0.0f, 1.0f, 0.12533289f, 0.9921147f, 0.0f,
		2.5066578f, 19.842295f, -20.0f, 0.0f, 0.0f, -1.0f, 0.12533289f, 0.9921147f, 0.0f,
		1.2558029f, 19.960535f, -5.0f, 0.0f, 0.0f, 1.0f, 0.06279014f, 0.9980267f, 0.0f,
		1.2558029f, 19.960535f, -20.0f, 0.0f, 0.0f, -1.0f, 0.06279014f, 0.9980267f, 0.0f,
		-8.0267855E-6f, 20.0f, -5.0f, 0.0f, 0.0f, 1.0f, -4.0133926E-7f, 1.0f, 0.0f,
		-8.0267855E-6f, 20.0f, -20.0f, 0.0f, 0.0f, -1.0f, -4.0133926E-7f, 1.0f, 0.0f,
		-1.255819f, 19.960533f, -5.0f, 0.0f, 0.0f, 1.0f, -0.062790945f, 0.99802667f, 0.0f,
		-1.255819f, 19.960533f, -20.0f, 0.0f, 0.0f, -1.0f, -0.062790945f, 0.99802667f, 0.0f,
		-2.5066736f, 19.842293f, -5.0f, 0.0f, 0.0f, 1.0f, -0.12533368f, 0.99211466f, 0.0f,
		-2.5066736f, 19.842293f, -20.0f, 0.0f, 0.0f, -1.0f, -0.12533368f, 0.99211466f, 0.0f,
		-3.7476356f, 19.645742f, -5.0f, 0.0f, 0.0f, 1.0f, -0.18738177f, 0.9822871f, 0.0f,
		-3.7476356f, 19.645742f, -20.0f, 0.0f, 0.0f, -1.0f, -0.18738177f, 0.9822871f, 0.0f,
		-4.9738073f, 19.37166f, -5.0f, 0.0f, 0.0f, 1.0f, -0.24869037f, 0.968583f, 0.0f,
		-4.9738073f, 19.37166f, -20.0f, 0.0f, 0.0f, -1.0f, -0.24869037f, 0.968583f, 0.0f,
		-6.18035f, 19.021128f, -5.0f, 0.0f, 0.0f, 1.0f, -0.30901748f, 0.95105636f, 0.0f,
		-6.18035f, 19.021128f, -20.0f, 0.0f, 0.0f, -1.0f, -0.30901748f, 0.95105636f, 0.0f,
		-7.362501f, 18.595526f, -5.0f, 0.0f, 0.0f, 1.0f, -0.36812505f, 0.9297763f, 0.0f,
		-7.362501f, 18.595526f, -20.0f, 0.0f, 0.0f, -1.0f, -0.36812505f, 0.9297763f, 0.0f,
		-8.515596f, 18.096537f, -5.0f, 0.0f, 0.0f, 1.0f, -0.42577982f, 0.9048268f, 0.0f,
		-8.515596f, 18.096537f, -20.0f, 0.0f, 0.0f, -1.0f, -0.42577982f, 0.9048268f, 0.0f,
		-9.635084f, 17.526127f, -5.0f, 0.0f, 0.0f, 1.0f, -0.4817542f, 0.87630635f, 0.0f,
		-9.635084f, 17.526127f, -20.0f, 0.0f, 0.0f, -1.0f, -0.4817542f, 0.87630635f, 0.0f,
		-10.716547f, 16.88655f, -5.0f, 0.0f, 0.0f, 1.0f, -0.53582734f, 0.84432757f, 0.0f,
		-10.716547f, 16.88655f, -20.0f, 0.0f, 0.0f, -1.0f, -0.53582734f, 0.84432757f, 0.0f,
		-11.755715f, 16.180332f, -5.0f, 0.0f, 0.0f, 1.0f, -0.5877858f, 0.8090166f, 0.0f,
		-11.755715f, 16.180332f, -20.0f, 0.0f, 0.0f, -1.0f, -0.5877858f, 0.8090166f, 0.0f,
		-12.74849f, 15.410256f, -5.0f, 0.0f, 0.0f, 1.0f, -0.6374245f, 0.7705128f, 0.0f,
		-12.74849f, 15.410256f, -20.0f, 0.0f, 0.0f, -1.0f, -0.6374245f, 0.7705128f, 0.0f,
		-13.690952f, 14.579363f, -5.0f, 0.0f, 0.0f, 1.0f, -0.6845476f, 0.72896814f, 0.0f,
		-13.690952f, 14.579363f, -20.0f, 0.0f, 0.0f, -1.0f, -0.6845476f, 0.72896814f, 0.0f,
		-14.579383f, 13.690931f, -5.0f, 0.0f, 0.0f, 1.0f, -0.72896916f, 0.6845466f, 0.0f,
		-14.579383f, 13.690931f, -20.0f, 0.0f, 0.0f, -1.0f, -0.72896916f, 0.6845466f, 0.0f,
		-15.4102745f, 12.748468f, -5.0f, 0.0f, 0.0f, 1.0f, -0.7705137f, 0.6374234f, 0.0f,
		-15.4102745f, 12.748468f, -20.0f, 0.0f, 0.0f, -1.0f, -0.7705137f, 0.6374234f, 0.0f,
		-16.18035f, 11.7556925f, -5.0f, 0.0f, 0.0f, 1.0f, -0.8090175f, 0.58778465f, 0.0f,
		-16.18035f, 11.7556925f, -20.0f, 0.0f, 0.0f, -1.0f, -0.8090175f, 0.58778465f, 0.0f,
		-16.886568f, 10.716522f, -5.0f, 0.0f, 0.0f, 1.0f, -0.8443284f, 0.5358261f, 0.0f,
		-16.886568f, 10.716522f, -20.0f, 0.0f, 0.0f, -1.0f, -0.8443284f, 0.5358261f, 0.0f,
		-17.526142f, 9.635058f, -5.0f, 0.0f, 0.0f, 1.0f, -0.87630713f, 0.48175293f, 0.0f,
		-17.526142f, 9.635058f, -20.0f, 0.0f, 0.0f, -1.0f, -0.87630713f, 0.48175293f, 0.0f,
		-18.096548f, 8.515571f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9048274f, 0.42577854f, 0.0f,
		-18.096548f, 8.515571f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9048274f, 0.42577854f, 0.0f,
		-18.595537f, 7.3624744f, -5.0f, 0.0f, 0.0f, 1.0f, -0.92977685f, 0.3681237f, 0.0f,
		-18.595537f, 7.3624744f, -20.0f, 0.0f, 0.0f, -1.0f, -0.92977685f, 0.3681237f, 0.0f,
		-19.021135f, 6.1803226f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9510568f, 0.30901614f, 0.0f,
		-19.021135f, 6.1803226f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9510568f, 0.30901614f, 0.0f,
		-19.371668f, 4.9737797f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9685834f, 0.24868898f, 0.0f,
		-19.371668f, 4.9737797f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9685834f, 0.24868898f, 0.0f,
		-19.645748f, 3.7476072f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9822874f, 0.18738036f, 0.0f,
		-19.645748f, 3.7476072f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9822874f, 0.18738036f, 0.0f,
		-19.842297f, 2.506645f, -5.0f, 0.0f, 0.0f, 1.0f, -0.99211484f, 0.12533225f, 0.0f,
		-19.842297f, 2.506645f, -20.0f, 0.0f, 0.0f, -1.0f, -0.99211484f, 0.12533225f, 0.0f,
		-19.960535f, 1.2557901f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9980267f, 0.06278951f, 0.0f,
		-19.960535f, 1.2557901f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9980267f, 0.06278951f, 0.0f,
		-20.0f, -2.0821943E-5f, -5.0f, 0.0f, 0.0f, 1.0f, -1.0f, -1.0410971E-6f, 0.0f,
		-20.0f, -2.0821943E-5f, -20.0f, 0.0f, 0.0f, -1.0f, -1.0f, -1.0410971E-6f, 0.0f,
		-19.960533f, -1.2558317f, -5.0f, 0.0f, 0.0f, 1.0f, -0.99802667f, -0.062791586f, 0.0f,
		-19.960533f, -1.2558317f, -20.0f, 0.0f, 0.0f, -1.0f, -0.99802667f, -0.062791586f, 0.0f,
		-19.84229f, -2.5066864f, -5.0f, 0.0f, 0.0f, 1.0f, -0.99211454f, -0.12533432f, 0.0f,
		-19.84229f, -2.5066864f, -20.0f, 0.0f, 0.0f, -1.0f, -0.99211454f, -0.12533432f, 0.0f,
		-19.64574f, -3.7476482f, -5.0f, 0.0f, 0.0f, 1.0f, -0.98228705f, -0.18738241f, 0.0f,
		-19.64574f, -3.7476482f, -20.0f, 0.0f, 0.0f, -1.0f, -0.98228705f, -0.18738241f, 0.0f,
		-19.371658f, -4.9738197f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9685829f, -0.248691f, 0.0f,
		-19.371658f, -4.9738197f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9685829f, -0.248691f, 0.0f,
		-19.021124f, -6.180362f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9510562f, -0.3090181f, 0.0f,
		-19.021124f, -6.180362f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9510562f, -0.3090181f, 0.0f,
		-18.59552f, -7.362513f, -5.0f, 0.0f, 0.0f, 1.0f, -0.929776f, -0.36812565f, 0.0f,
		-18.59552f, -7.362513f, -20.0f, 0.0f, 0.0f, -1.0f, -0.929776f, -0.36812565f, 0.0f,
		-18.09653f, -8.515608f, -5.0f, 0.0f, 0.0f, 1.0f, -0.9048265f, -0.4257804f, 0.0f,
		-18.09653f, -8.515608f, -20.0f, 0.0f, 0.0f, -1.0f, -0.9048265f, -0.4257804f, 0.0f,
		-17.526121f, -9.635096f, -5.0f, 0.0f, 0.0f, 1.0f, -0.87630606f, -0.48175478f, 0.0f,
		-17.526121f, -9.635096f, -20.0f, 0.0f, 0.0f, -1.0f, -0.87630606f, -0.48175478f, 0.0f,
		-16.886545f, -10.7165575f, -5.0f, 0.0f, 0.0f, 1.0f, -0.8443273f, -0.5358279f, 0.0f,
		-16.886545f, -10.7165575f, -20.0f, 0.0f, 0.0f, -1.0f, -0.8443273f, -0.5358279f, 0.0f,
		-16.180325f, -11.755726f, -5.0f, 0.0f, 0.0f, 1.0f, -0.8090162f, -0.5877863f, 0.0f,
		-16.180325f, -11.755726f, -20.0f, 0.0f, 0.0f, -1.0f, -0.8090162f, -0.5877863f, 0.0f,
		-15.410248f, -12.7485f, -5.0f, 0.0f, 0.0f, 1.0f, -0.7705124f, -0.637425f, 0.0f,
		-15.410248f, -12.7485f, -20.0f, 0.0f, 0.0f, -1.0f, -0.7705124f, -0.637425f, 0.0f,
		-14.579354f, -13.690962f, -5.0f, 0.0f, 0.0f, 1.0f, -0.7289677f, -0.6845481f, 0.0f,
		-14.579354f, -13.690962f, -20.0f, 0.0f, 0.0f, -1.0f, -0.7289677f, -0.6845481f, 0.0f,
		-13.690922f, -14.5793915f, -5.0f, 0.0f, 0.0f, 1.0f, -0.6845461f, -0.7289696f, 0.0f,
		-13.690922f, -14.5793915f, -20.0f, 0.0f, 0.0f, -1.0f, -0.6845461f, -0.7289696f, 0.0f,
		-12.748462f, -15.410279f, -5.0f, 0.0f, 0.0f, 1.0f, -0.6374231f, -0.77051395f, 0.0f,
		-12.748462f, -15.410279f, -20.0f, 0.0f, 0.0f, -1.0f, -0.6374231f, -0.77051395f, 0.0f,
		-11.755686f, -16.180353f, -5.0f, 0.0f, 0.0f, 1.0f, -0.5877843f, -0.80901766f, 0.0f,
		-11.755686f, -16.180353f, -20.0f, 0.0f, 0.0f, -1.0f, -0.5877843f, -0.80901766f, 0.0f,
		-10.716516f, -16.886572f, -5.0f, 0.0f, 0.0f, 1.0f, -0.5358258f, -0.8443286f, 0.0f,
		-10.716516f, -16.886572f, -20.0f, 0.0f, 0.0f, -1.0f, -0.5358258f, -0.8443286f, 0.0f,
		-9.635052f, -17.526146f, -5.0f, 0.0f, 0.0f, 1.0f, -0.48175257f, -0.8763073f, 0.0f,
		-9.635052f, -17.526146f, -20.0f, 0.0f, 0.0f, -1.0f, -0.48175257f, -0.8763073f, 0.0f,
		-8.515563f, -18.096552f, -5.0f, 0.0f, 0.0f, 1.0f, -0.42577815f, -0.9048276f, 0.0f,
		-8.515563f, -18.096552f, -20.0f, 0.0f, 0.0f, -1.0f, -0.42577815f, -0.9048276f, 0.0f,
		-7.3624673f, -18.59554f, -5.0f, 0.0f, 0.0f, 1.0f, -0.36812335f, -0.92977697f, 0.0f,
		-7.3624673f, -18.59554f, -20.0f, 0.0f, 0.0f, -1.0f, -0.36812335f, -0.92977697f, 0.0f,
		-6.180315f, -19.02114f, -5.0f, 0.0f, 0.0f, 1.0f, -0.30901575f, -0.95105696f, 0.0f,
		-6.180315f, -19.02114f, -20.0f, 0.0f, 0.0f, -1.0f, -0.30901575f, -0.95105696f, 0.0f,
		-4.9737716f, -19.37167f, -5.0f, 0.0f, 0.0f, 1.0f, -0.24868858f, -0.96858346f, 0.0f,
		-4.9737716f, -19.37167f, -20.0f, 0.0f, 0.0f, -1.0f, -0.24868858f, -0.96858346f, 0.0f,
		-3.7475994f, -19.64575f, -5.0f, 0.0f, 0.0f, 1.0f, -0.18737997f, -0.9822875f, 0.0f,
		-3.7475994f, -19.64575f, -20.0f, 0.0f, 0.0f, -1.0f, -0.18737997f, -0.9822875f, 0.0f,
		-2.506637f, -19.842297f, -5.0f, 0.0f, 0.0f, 1.0f, -0.12533185f, -0.99211484f, 0.0f,
		-2.506637f, -19.842297f, -20.0f, 0.0f, 0.0f, -1.0f, -0.12533185f, -0.99211484f, 0.0f,
		-1.2557821f, -19.960537f, -5.0f, 0.0f, 0.0f, 1.0f, -0.062789105f, -0.99802685f, 0.0f,
		-1.2557821f, -19.960537f, -20.0f, 0.0f, 0.0f, -1.0f, -0.062789105f, -0.99802685f, 0.0f,
		2.8848726E-5f, -20.0f, -5.0f, 0.0f, 0.0f, 1.0f, 1.4424363E-6f, -1.0f, 0.0f,
		2.8848726E-5f, -20.0f, -20.0f, 0.0f, 0.0f, -1.0f, 1.4424363E-6f, -1.0f, 0.0f,
		1.2558397f, -19.960533f, -5.0f, 0.0f, 0.0f, 1.0f, 0.06279199f, -0.99802667f, 0.0f,
		1.2558397f, -19.960533f, -20.0f, 0.0f, 0.0f, -1.0f, 0.06279199f, -0.99802667f, 0.0f,
		2.5066943f, -19.84229f, -5.0f, 0.0f, 0.0f, 1.0f, 0.12533471f, -0.99211454f, 0.0f,
		2.5066943f, -19.84229f, -20.0f, 0.0f, 0.0f, -1.0f, 0.12533471f, -0.99211454f, 0.0f,
		3.747656f, -19.645739f, -5.0f, 0.0f, 0.0f, 1.0f, 0.1873828f, -0.98228693f, 0.0f,
		3.747656f, -19.645739f, -20.0f, 0.0f, 0.0f, -1.0f, 0.1873828f, -0.98228693f, 0.0f,
		4.973828f, -19.371656f, -5.0f, 0.0f, 0.0f, 1.0f, 0.2486914f, -0.9685828f, 0.0f,
		4.973828f, -19.371656f, -20.0f, 0.0f, 0.0f, -1.0f, 0.2486914f, -0.9685828f, 0.0f,
		6.18037f, -19.02112f, -5.0f, 0.0f, 0.0f, 1.0f, 0.3090185f, -0.951056f, 0.0f,
		6.18037f, -19.02112f, -20.0f, 0.0f, 0.0f, -1.0f, 0.3090185f, -0.951056f, 0.0f,
		7.3625207f, -18.595518f, -5.0f, 0.0f, 0.0f, 1.0f, 0.36812603f, -0.9297759f, 0.0f,
		7.3625207f, -18.595518f, -20.0f, 0.0f, 0.0f, -1.0f, 0.36812603f, -0.9297759f, 0.0f,
		8.515615f, -18.096527f, -5.0f, 0.0f, 0.0f, 1.0f, 0.42578077f, -0.90482634f, 0.0f,
		8.515615f, -18.096527f, -20.0f, 0.0f, 0.0f, -1.0f, 0.42578077f, -0.90482634f, 0.0f,
		9.635102f, -17.526117f, -5.0f, 0.0f, 0.0f, 1.0f, 0.4817551f, -0.8763059f, 0.0f,
		9.635102f, -17.526117f, -20.0f, 0.0f, 0.0f, -1.0f, 0.4817551f, -0.8763059f, 0.0f,
		10.716564f, -16.886541f, -5.0f, 0.0f, 0.0f, 1.0f, 0.53582823f, -0.8443271f, 0.0f,
		10.716564f, -16.886541f, -20.0f, 0.0f, 0.0f, -1.0f, 0.53582823f, -0.8443271f, 0.0f,
		11.755733f, -16.18032f, -5.0f, 0.0f, 0.0f, 1.0f, 0.5877866f, -0.80901605f, 0.0f,
		11.755733f, -16.18032f, -20.0f, 0.0f, 0.0f, -1.0f, 0.5877866f, -0.80901605f, 0.0f,
		12.748507f, -15.410243f, -5.0f, 0.0f, 0.0f, 1.0f, 0.6374253f, -0.77051216f, 0.0f,
		12.748507f, -15.410243f, -20.0f, 0.0f, 0.0f, -1.0f, 0.6374253f, -0.77051216f, 0.0f,
		13.690968f, -14.579349f, -5.0f, 0.0f, 0.0f, 1.0f, 0.6845484f, -0.7289674f, 0.0f,
		13.690968f, -14.579349f, -20.0f, 0.0f, 0.0f, -1.0f, 0.6845484f, -0.7289674f, 0.0f,
		14.579397f, -13.690916f, -5.0f, 0.0f, 0.0f, 1.0f, 0.7289699f, -0.6845458f, 0.0f,
		14.579397f, -13.690916f, -20.0f, 0.0f, 0.0f, -1.0f, 0.7289699f, -0.6845458f, 0.0f,
		15.410288f, -12.748452f, -5.0f, 0.0f, 0.0f, 1.0f, 0.77051437f, -0.6374226f, 0.0f,
		15.410288f, -12.748452f, -20.0f, 0.0f, 0.0f, -1.0f, 0.77051437f, -0.6374226f, 0.0f,
		16.18036f, -11.755675f, -5.0f, 0.0f, 0.0f, 1.0f, 0.809018f, -0.58778375f, 0.0f,
		16.18036f, -11.755675f, -20.0f, 0.0f, 0.0f, -1.0f, 0.809018f, -0.58778375f, 0.0f,
		16.886578f, -10.716505f, -5.0f, 0.0f, 0.0f, 1.0f, 0.8443289f, -0.53582525f, 0.0f,
		16.886578f, -10.716505f, -20.0f, 0.0f, 0.0f, -1.0f, 0.8443289f, -0.53582525f, 0.0f,
		17.526152f, -9.63504f, -5.0f, 0.0f, 0.0f, 1.0f, 0.8763076f, -0.481752f, 0.0f,
		17.526152f, -9.63504f, -20.0f, 0.0f, 0.0f, -1.0f, 0.8763076f, -0.481752f, 0.0f,
		18.096558f, -8.515552f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9048279f, -0.42577758f, 0.0f,
		18.096558f, -8.515552f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9048279f, -0.42577758f, 0.0f,
		18.595545f, -7.3624554f, -5.0f, 0.0f, 0.0f, 1.0f, 0.92977726f, -0.36812276f, 0.0f,
		18.595545f, -7.3624554f, -20.0f, 0.0f, 0.0f, -1.0f, 0.92977726f, -0.36812276f, 0.0f,
		19.021143f, -6.1803026f, -5.0f, 0.0f, 0.0f, 1.0f, 0.95105714f, -0.30901513f, 0.0f,
		19.021143f, -6.1803026f, -20.0f, 0.0f, 0.0f, -1.0f, 0.95105714f, -0.30901513f, 0.0f,
		19.371674f, -4.973759f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9685837f, -0.24868795f, 0.0f,
		19.371674f, -4.973759f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9685837f, -0.24868795f, 0.0f,
		19.645752f, -3.747587f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9822876f, -0.18737935f, 0.0f,
		19.645752f, -3.747587f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9822876f, -0.18737935f, 0.0f,
		19.842299f, -2.5066245f, -5.0f, 0.0f, 0.0f, 1.0f, 0.9921149f, -0.12533122f, 0.0f,
		19.842299f, -2.5066245f, -20.0f, 0.0f, 0.0f, -1.0f, 0.9921149f, -0.12533122f, 0.0f,
		19.960537f, -1.2557694f, -5.0f, 0.0f, 0.0f, 1.0f, 0.99802685f, -0.06278847f, 0.0f,
		19.960537f, -1.2557694f, -20.0f, 0.0f, 0.0f, -1.0f, 0.99802685f, -0.06278847f, 0.0f,
		20.0f, 4.1643885E-5f, -5.0f, 0.0f, 0.0f, 1.0f, 1.0f, 2.0821942E-6f, 0.0f,
		// </editor-fold>
		20.0f, 4.1643885E-5f, -20.0f, 0.0f, 0.0f, -1.0f, 1.0f, 2.0821942E-6f, 0.0f};
	private static int vertShadID = 0;
	private static final String vertexShaderCode = OpenGLEngine.readFile(Pizza.class.getResourceAsStream("/OpenGLPanel/pizza.vert"), Charset.defaultCharset());
	private static int fragShadID = 0;
	private static final String fragmentShaderCode = OpenGLEngine.readFile(Pizza.class.getResourceAsStream("/OpenGLPanel/pizza.frag"), Charset.defaultCharset());
	private static int shader = 0;
	private static int pointerColor = 0;

	private static final float pizzaRadius = 20;
	private static float pizzaScale = 1;
	private static final float pizzaSelHeight = -200;
	private static float pizzaSelScale = 5;
	private static final int maxSelPizzas = 6;
	private static int noSelPizzas = 0;
	private static final int animLenght = 30; // the lenght of time to display the animation, in frames.

	static void setPizzaRadius(float radius) {
		pizzaScale = radius / pizzaRadius;
		pizzaSelScale = pizzaScale * 3;
	}

	public static float getPizzaRadius() {
		return pizzaSelScale * pizzaRadius;
	}

	private static void createVBOAndShaders() throws Exception {
		if (vboID == 0) {
			vboID = GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
			FloatBuffer fb = BufferUtils.createFloatBuffer(vertices.length);
			fb.put(vertices);
			fb.rewind();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
		}
		if (vertShadID == 0) {
			vertShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
			if (vertShadID == 0) {
				throw new Exception("Cannot create the vertex shader.");
			}
			ARBShaderObjects.glShaderSourceARB(vertShadID, vertexShaderCode);
			ARBShaderObjects.glCompileShaderARB(vertShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(vertShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(vertShadID);
				vertShadID = 0;
				throw new Exception("Cannot compile vertex shader.");
			}
		}
		if (fragShadID == 0) {
			fragShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
			if (fragShadID == 0) {
				throw new Exception("Cannot create the fragment shader.");
			}
			ARBShaderObjects.glShaderSourceARB(fragShadID, fragmentShaderCode);
			ARBShaderObjects.glCompileShaderARB(fragShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(fragShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(fragShadID);
				fragShadID = 0;
				throw new Exception("Cannot compile fragment shader.");
			}
		}
		if (shader == 0) {
			shader = ARBShaderObjects.glCreateProgramObjectARB();
			if (shader == 0) {
				throw new Exception("Cannot create the shader program.");
			}
			ARBShaderObjects.glAttachObjectARB(shader, vertShadID);
			ARBShaderObjects.glAttachObjectARB(shader, fragShadID);
			ARBShaderObjects.glLinkProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error linking the shader.");
			}
			ARBShaderObjects.glValidateProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error validating the shader.");
			}
			pointerColor = ARBShaderObjects.glGetUniformLocationARB(shader, "color");
		}
	}

	private final float[] percents;
	private final Color[] cs;
	private final int xC, yC;// the parameter passed by the user, aka the default position.
	private boolean selected = false;
	private float x, y, z;// the actual position in this frame.
	private float xT, yT, zT;// the target position in this frame.
	private float xD = 0, yD = 0, zD = 0;// the delta values. to be applied each frame until the actual position is in the target.
	private float s = pizzaScale;
	private float sT = pizzaScale;
	private float sD = 0;
	private int textureID = 0;

	public Pizza(int xC, int yC, Color[] colors, float[] percents) throws IllegalArgumentException {
		if (colors == null || percents == null || colors.length != percents.length) {
			throw new IllegalArgumentException("Both arrays have to have the same size.");
		}
		this.percents = percents;
		this.xC = xC;
		this.yC = yC;
		x = xC;
		y = yC;
		z = 0;
		xT = xC;
		yT = yC;
		zT = 0;
		cs = colors;
	}

	public Pizza(int xC, int yC, Color[] colors, int[] quantities) {
		if (colors == null || quantities == null || colors.length != quantities.length) {
			throw new IllegalArgumentException("Both arrays have to have the same size.");
		}
		float sum = 0;
		for (int q : quantities) {
			sum += q;
		}
		float[] percs = new float[quantities.length];
		for (int i = 0; i < percs.length; i++) {
			percs[i] = (float) quantities[i] / sum;
		}
		percents = percs;
		this.xC = xC;
		this.yC = yC;
		x = xC;
		y = yC;
		z = 0;
		xT = xC;
		yT = yC;
		zT = 0;
		cs = colors;
	}

	protected void destroy() {
		if (vboID != 0) {
			GL15.glDeleteBuffers(vboID);
			vboID = 0;
		}
		if (shader != 0) {
			ARBShaderObjects.glDeleteObjectARB(shader);
			shader = 0;
			pointerColor = 0;
		}
		if (vertShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(vertShadID);
			vertShadID = 0;
		}
		if (fragShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(fragShadID);
			fragShadID = 0;
		}
		if (textureID != 0) {
			GL11.glDeleteTextures(textureID);
			textureID = 0;
		}
	}

	protected void resetSize() {
		s = pizzaScale;
		sT = pizzaScale;
	}

	private void calcColors(Color[] cs) {
		if (textureID != 0) {
			return;
		}
		int idx = 0;
		float percent = percents[0];
		ByteBuffer data = ByteBuffer.allocateDirect(4 * 256);
		for (int i = 0; i < 256; i++) {
			if (percent < ((float) i) / 255.0) {
				idx++;
				if (idx >= percents.length) {
					idx = percents.length - 1;
				}
				percent += percents[idx];
			}
			data.put((byte) cs[idx].getRed());
			data.put((byte) cs[idx].getGreen());
			data.put((byte) cs[idx].getBlue());
			data.put((byte) 255);
		}
		data.rewind();
		textureID = GL11.glGenTextures();
		GL11.glBindTexture(GL11.GL_TEXTURE_1D, textureID);
		GL11.glTexParameteri(GL11.GL_TEXTURE_1D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_1D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexImage1D(GL11.GL_TEXTURE_1D, 0, GL11.GL_RGBA, 256, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, data);
		GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 4);
		GL11.glBindTexture(GL11.GL_TEXTURE_1D, 0);
	}

	protected void draw() throws Exception {
		draw(null);
	}

	protected void draw(Byte color) throws Exception {
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, z);
		GL11.glScalef(s, s, s);
		createVBOAndShaders();
		calcColors(cs);
		if (color == null) {
			GL11.glColor4ub((byte) 255, (byte) 0, (byte) 0, (byte) 0);
		} else {
			GL11.glColor4ub(color, color, color, (byte) 255);
		}
		ARBShaderObjects.glUseProgramObjectARB(shader);
		GL11.glBindTexture(GL11.GL_TEXTURE_1D, textureID);
		ARBShaderObjects.glUniform1iARB(pointerColor, 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		GL11.glVertexPointer(3, GL11.GL_FLOAT, 3 * 3 * 4 * 2, 0);// disc
		GL11.glNormalPointer(GL11.GL_FLOAT, 3 * 3 * 4 * 2, 3 * 4);
		GL11.glDrawArrays(GL11.GL_TRIANGLE_FAN, 0, 102);
		GL11.glVertexPointer(3, GL11.GL_FLOAT, 3 * 3 * 4, 3 * 3 * 4);// strip
		GL11.glNormalPointer(GL11.GL_FLOAT, 3 * 3 * 4, 3 * 3 * 4 + 3 * 4 * 2);
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 202);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL11.glPopMatrix();
		GL11.glBindTexture(GL11.GL_TEXTURE_1D, 0);
		ARBShaderObjects.glUseProgramObjectARB(0);
		if (selected) {
			GL11.glColor3f(0, 0, 0);
			GL11.glBegin(GL11.GL_LINES);
			GL11.glVertex3f(xC, yC, 0);
			GL11.glVertex3f(x, y, z);
			GL11.glEnd();
		}
		if (color == null) {
			updatePositions();
		}
	}

	void toggleSelected(float x, float y) {
		float r = pizzaRadius * s;
		float dx = x - this.x;
		float dy = y - this.y;
		if (r >= Math.sqrt(dx * dx + dy * dy)) {
			toggleSelected();
		}
	}

	void toggleSelected() {
		if (selected) {// resets the render position.
			xT = xC;
			yT = yC;
			zT = 0;
			sT = pizzaScale;
			noSelPizzas--;
			if (noSelPizzas < 0) {
				noSelPizzas = 0;
			}
		} else {
			if (maxSelPizzas == noSelPizzas) {
				return;
			}
			zT = pizzaSelHeight;
			sT = pizzaSelScale;
			noSelPizzas++;
		}
		selected = !selected;
		updateDeltas();
	}

	public boolean isSelected() {
		return selected;
	}

	float getXC() {
		return xC;
	}

	float getYC() {
		return yC;
	}

	void setXYSel(float x, float y) {
		xT = x;
		yT = y;
		updateDeltas();
	}

	private void updateDeltas() {
		xD = (xT - x) / animLenght;
		yD = (yT - y) / animLenght;
		zD = (zT - z) / animLenght;
		sD = (sT - s) / animLenght;
	}

	private void updatePositions() {
		if (xD > 0) {
			if (x < xT) {
				x += xD;
			} else {
				xD = 0;
			}
		} else if (xD < 0) {
			if (x > xT) {
				x += xD;
			} else {
				xD = 0;
			}
		}
		if (yD > 0) {
			if (y < yT) {
				y += yD;
			} else {
				yD = 0;
			}
		} else if (yD < 0) {
			if (y > yT) {
				y += yD;
			} else {
				yD = 0;
			}
		}
		if (zD > 0) {
			if (z < zT) {
				z += zD;
			} else {
				zD = 0;
			}
		} else if (zD < 0) {
			if (z > zT) {
				z += zD;
			} else {
				zD = 0;
			}
		}
		if (sD > 0) {
			if (s < sT) {
				s += sD;
			} else {
				sD = 0;
			}
		} else if (sD < 0) {
			if (s > sT) {
				s += sD;
			} else {
				sD = 0;
			}
		}
	}
}
