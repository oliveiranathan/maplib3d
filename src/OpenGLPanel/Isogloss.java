/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
package OpenGLPanel;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 *
 * @author Nathan Oliveira
 */
public class Isogloss {

	private static final int animLenght = 60; // the lenght of time to display the animation, in frames.
	private static int vertShadID = 0;
	private static final String vertexShaderCode = OpenGLEngine.readFile(Isogloss.class.getResourceAsStream("/OpenGLPanel/isogloss.vert"), Charset.defaultCharset());
	private static int fragShadID = 0;
	private static final String fragmentShaderCode = OpenGLEngine.readFile(Isogloss.class.getResourceAsStream("/OpenGLPanel/isogloss.frag"), Charset.defaultCharset());
	private static int shader = 0;
	private static int pointerMask = 0;
	private static Texture textureMask = null;
	private static int pointerSize = 0;
	private static int pointerProp = 0;
	private static int pointerAlpha = 0;
	private static int pointerSel = 0;

	private int vboID = 0;
	private float[] vertices = null;
	private final boolean[][] mask;
	private BufferedImage maskImg = null;
	private final int w, h;
	private final float s, t;
	private final List<Point2D.Double> pointsMap;
	private final List<Point2D.Double> pointsSelected;

	private boolean selected = false;
	private final float aC;
	private float aT, alpha;
	private float aD = 0;
	private final int r, g, b;

	public Isogloss(Color color, boolean[][] mask, int w, int h, List<Point2D.Double> pointsMap, List<Point2D.Double> pointsSelected) {
		aC = 0.8f;
		r = color.getRed();
		g = color.getGreen();
		b = color.getBlue();
		aT = aC;
		alpha = aC;
		this.mask = mask;
		this.pointsMap = pointsMap;
		this.pointsSelected = pointsSelected;
		this.w = w;
		this.h = h;
		s = (float) w / OpenGLEngine.nextPo2(w);
		t = (float) h / OpenGLEngine.nextPo2(h);
	}

	public Isogloss(Color color, BufferedImage mask, int w, int h, List<Point2D.Double> pointsMap, List<Point2D.Double> pointsSelected) {
		aC = 0.8f;
		r = color.getRed();
		g = color.getGreen();
		b = color.getBlue();
		aT = aC;
		alpha = aC;
		this.mask = null;
		maskImg = mask;
		this.pointsMap = pointsMap;
		this.pointsSelected = pointsSelected;
		this.w = w;
		this.h = h;
		s = (float) w / OpenGLEngine.nextPo2(w);
		t = (float) h / OpenGLEngine.nextPo2(h);
	}

	protected void draw() throws Exception {
		draw(null);
	}

	protected void draw(Byte color) throws Exception {
		createVBOAndShaders();
		GL11.glPushMatrix();
		GL11.glTranslatef(-w / 2, -h / 2, 0);
		if (color == null) {
			GL11.glColor4ub((byte) r, (byte) g, (byte) b, (byte) 0);
		} else {
			GL11.glColor4ub(color, color, color, (byte) 255);
		}
		ARBShaderObjects.glUseProgramObjectARB(shader);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureMask.getTextureID());
		ARBShaderObjects.glUniform1iARB(pointerMask, 0);
		ARBShaderObjects.glUniform2fARB(pointerSize, w, h);
		ARBShaderObjects.glUniform2fARB(pointerProp, s, t);
		ARBShaderObjects.glUniform1fARB(pointerAlpha, alpha);
		ARBShaderObjects.glUniform1iARB(pointerSel, selected ? 1 : 0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
		GL11.glVertexPointer(3, GL11.GL_FLOAT, 3 * 4, 0);
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vertices.length / 3);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		ARBShaderObjects.glUseProgramObjectARB(0);
		GL11.glPopMatrix();
		if (color == null) {
			updateAlpha();
		}
	}

	@SuppressWarnings("ValueOfIncrementOrDecrementUsed")
	private void createVBOAndShaders() throws Exception {
		if (textureMask == null) {
			if (mask != null) {
				maskImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
				for (int x = 0; x < w; x++) {
					for (int y = 0; y < h; y++) {
						if (mask[x][y]) {
							maskImg.setRGB(x, y, Color.WHITE.getRGB());
						}
					}
				}
			}
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream(w * h * 4)) {
				ImageIO.write(maskImg, "PNG", baos);
				try (ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray())) {
					textureMask = TextureLoader.getTexture("PNG", bais);
				}
			} catch (IOException ex) {
				textureMask = null;
			}
		}
		if (vboID == 0) {
			vboID = GL15.glGenBuffers();
			ConcurrentHashMap<Point2D.Double, Polygon> polygons = calcPolygons();
			List<Polygon> ours = new ArrayList<>();
			pointsSelected.stream().forEach((p) -> {
				if (polygons.get(p) != null && polygons.get(p).npoints > 2) {
					ours.add(polygons.get(p));
				}
			});
			int vertexCount = 0;
			vertexCount = ours.stream().map((p) -> p.npoints).reduce(vertexCount, Integer::sum);
			vertices = new float[vertexCount * 3 * 3];
			int idx = 0;
			for (Point2D.Double p : pointsSelected) {
				for (int i = 0; i < polygons.get(p).npoints; i++) {
					vertices[idx++] = (float) p.x;
					vertices[idx++] = (float) p.y;
					vertices[idx++] = -0.1f;
					vertices[idx++] = polygons.get(p).xpoints[i];
					vertices[idx++] = polygons.get(p).ypoints[i];
					vertices[idx++] = -0.1f;
					vertices[idx++] = polygons.get(p).xpoints[(i + 1) % polygons.get(p).npoints];
					vertices[idx++] = polygons.get(p).ypoints[(i + 1) % polygons.get(p).npoints];
					vertices[idx++] = -0.1f;
				}
			}
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
			FloatBuffer fb = BufferUtils.createFloatBuffer(vertices.length);
			fb.put(vertices);
			fb.rewind();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
		}
		if (vertShadID == 0) {
			vertShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
			if (vertShadID == 0) {
				throw new Exception("Cannot create the vertex shader");
			}
			ARBShaderObjects.glShaderSourceARB(vertShadID, vertexShaderCode);
			ARBShaderObjects.glCompileShaderARB(vertShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(vertShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(vertShadID);
				vertShadID = 0;
				throw new Exception("Cannot compile vertex shader.");
			}
		}
		if (fragShadID == 0) {
			fragShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
			if (fragShadID == 0) {
				throw new Exception("Cannot create the fragment shader.");
			}
			ARBShaderObjects.glShaderSourceARB(fragShadID, fragmentShaderCode);
			ARBShaderObjects.glCompileShaderARB(fragShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(fragShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(fragShadID);
				fragShadID = 0;
				throw new Exception("Cannot compile fragment shader.");
			}
		}
		if (shader == 0) {
			shader = ARBShaderObjects.glCreateProgramObjectARB();
			if (shader == 0) {
				throw new Exception("Cannot create the shader program.");
			}
			ARBShaderObjects.glAttachObjectARB(shader, vertShadID);
			ARBShaderObjects.glAttachObjectARB(shader, fragShadID);
			ARBShaderObjects.glLinkProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error linking the shader.");
			}
			ARBShaderObjects.glValidateProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error validating the shader.");
			}
			pointerMask = ARBShaderObjects.glGetUniformLocationARB(shader, "mask");
			pointerSize = ARBShaderObjects.glGetUniformLocationARB(shader, "size");
			pointerProp = ARBShaderObjects.glGetUniformLocationARB(shader, "prop");
			pointerAlpha = ARBShaderObjects.glGetUniformLocationARB(shader, "alpha");
			pointerSel = ARBShaderObjects.glGetUniformLocationARB(shader, "isSelected");
		}
	}

	protected void destroy() {
		if (vboID != 0) {
			GL15.glDeleteBuffers(vboID);
			vboID = 0;
		}
		if (shader != 0) {
			ARBShaderObjects.glDeleteObjectARB(shader);
			shader = 0;
			pointerMask = 0;
			pointerSize = 0;
			pointerProp = 0;
			pointerAlpha = 0;
			pointerSel = 0;
		}
		if (vertShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(vertShadID);
			vertShadID = 0;
		}
		if (fragShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(fragShadID);
			fragShadID = 0;
		}
		if (textureMask != null) {
			textureMask.release();
			textureMask = null;
		}
	}

	protected void toggleSelected() {
		if (selected) {
			selected = false;
			aT = 0.8f;
			updateDelta();
		} else {
			selected = true;
			aT = 1.0f;
			updateDelta();
		}
	}

	private void updateDelta() {
		aD = (aT - alpha) / animLenght;
	}

	private void updateAlpha() {
		if (aD > 0) {
			if (alpha < aT) {
				alpha += aD;
			} else if (selected) {
				aT = 0.6f;
				updateDelta();
			} else {
				aT = 0.8f;
				updateDelta();
			}
		} else if (aD < 0) {
			if (alpha > aT) {
				alpha += aD;
			} else if (selected) {
				aT = 1.0f;
				updateDelta();
			} else {
				aT = 0.8f;
				updateDelta();
			}
		}
	}

	private ConcurrentHashMap<Point2D.Double, Polygon> calcPolygons() {
		ConcurrentHashMap<Point2D.Double, Polygon> list = new ConcurrentHashMap<>();
		pointsMap.stream().forEach((p1) -> {
			List<Line2D.Double> pToPLines = new ArrayList<>();
			pointsMap.stream().filter((p2) -> (p1 != p2)).forEach((p2)
					-> pToPLines.add(new Line2D.Double(p1, p2)));
			calcPerpendicularLines(pToPLines, p1, list);
		});
		return list;
	}

	private void calcPerpendicularLines(List<Line2D.Double> pToPLines, Point2D.Double p1, ConcurrentHashMap<Point2D.Double, Polygon> map) {
		List<Line2D.Double> lines = new ArrayList<>();
		pToPLines.stream().forEach((l) -> lines.add(extendLine(perpendicular(l))));
		map.put(p1, cutPolygon(lines, p1));
	}

	private Line2D.Double perpendicular(Line2D.Double l) {
		double halfDx = (l.x2 - l.x1) / 2;
		double halfDy = (l.y2 - l.y1) / 2;
		double xc = l.x1 + halfDx;
		double yc = l.y1 + halfDy;
		double x1 = l.x1 - xc;
		double x2 = l.x2 - xc;
		double y1 = l.y1 - yc;
		double y2 = l.y2 - yc;
		Line2D.Double result = new Line2D.Double(new Point2D.Double(xc + y1, yc - x1),
				new Point2D.Double(xc + y2, yc - x2));
		return result;
	}

	private Line2D.Double extendLine(Line2D.Double l) {
		Point2D.Double nB = new Point2D.Double();
		Point2D.Double nA = new Point2D.Double();
		double lenAB = l.getP1().distance(l.getP2());
		double maxLen = Math.sqrt(w * w + h * h);
		nB.x = l.x2 + (l.x2 - l.x1) / lenAB * maxLen;
		nB.y = l.y2 + (l.y2 - l.y1) / lenAB * maxLen;
		nA.x = l.x1 + (l.x1 - l.x2) / lenAB * maxLen;
		nA.y = l.y1 + (l.y1 - l.y2) / lenAB * maxLen;
		return new Line2D.Double(nA, nB);
	}

	private Polygon cutPolygon(List<Line2D.Double> lines, Point2D.Double center) {
		List<Point2D.Double> poly = new ArrayList<>();
		poly.add(new Point2D.Double(0, 0));
		poly.add(new Point2D.Double(w, 0));
		poly.add(new Point2D.Double(w, h));
		poly.add(new Point2D.Double(0, h));
		for (Line2D.Double line : lines) {
			List<Point2D.Double> vertexes = new ArrayList<>();
			for (int idx = 0; idx < poly.size(); idx++) {
				Point2D.Double p = poly.get(idx);
				Point2D.Double ant = (idx == 0) ? poly.get(poly.size() - 1) : poly.get(idx - 1);
				if (!line.intersectsLine(new Line2D.Double(p, center))) {
					if (line.intersectsLine(new Line2D.Double(ant, center))) {
						vertexes.add(intersect(line, ant, p));
					}
					vertexes.add(p);
				} else if (!line.intersectsLine(new Line2D.Double(ant, center))) {
					vertexes.add(intersect(line, ant, p));
				}
			}
			poly = vertexes;
		}
		int[] xs = new int[poly.size()];
		int[] ys = new int[poly.size()];
		for (int i = 0; i < poly.size(); i++) {
			xs[i] = (int) Math.round(poly.get(i).x);
			ys[i] = (int) Math.round(poly.get(i).y);
		}
		return new Polygon(xs, ys, xs.length);
	}

	private Point2D.Double intersect(Line2D.Double line, Point2D.Double p3, Point2D.Double p4) {
		double x12 = line.x1 - line.x2;
		double x34 = p3.x - p4.x;
		double y12 = line.y1 - line.y2;
		double y34 = p3.y - p4.y;
		double c = x12 * y34 - y12 * x34;
		double a = line.x1 * line.y2 - line.y1 * line.x2;
		@SuppressWarnings("LocalVariableHidesMemberVariable")
		double b = p3.x * p4.y - p3.y * p4.x;
		double x = (a * x34 - b * x12) / c;
		double y = (a * y34 - b * y12) / c;
		Point2D.Double ret = new Point2D.Double(x, y);
		return ret;
	}
}
