/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
package OpenGLPanel;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 *
 * @author Nathan Oliveira
 */
public class OpenGLPanel {

	private OpenGLEngine engine;
	private final Thread thread;
	private Exception exception = null;
	private List<Pizza> pizzas = new ArrayList<>();
	private List<Isogloss> isogloss = new ArrayList<>();
	private final OpenGLPanel bigThis = this;
	private Gradient gradient = null;
	private Color[] colors = null;
	private String[] labels = null;
	private float[] floats = null;
	private int[] integers = null;
	private int[] cIdx = null;

	public OpenGLPanel(BufferedImage map, boolean fullscreen) throws Exception {
		thread = new Thread(() -> {
			try {
				engine = new OpenGLEngine(map, bigThis, fullscreen);
			} catch (Exception ex) {
				exception = ex;
				return;
			}
			try {
				engine.start();
			} catch (Exception ex) {
				Logger.getLogger(OpenGLPanel.class.getName()).log(Level.SEVERE, null, ex);
			}
		});
		if (exception != null) {
			throw exception;
		}
	}

	public void start() {
		if (!isRunning()) {
			thread.start();
		}
	}

	public void stop() {
		if (isRunning()) {
			engine.stop();
		}
	}

	public boolean isRunning() {
		return thread.isAlive();
	}

	public void addPizzas(List<Pizza> pizzas) {
		this.pizzas.addAll(pizzas);
	}

	protected boolean hasPizzas() {
		return !pizzas.isEmpty();
	}

	protected List<Pizza> getPizzas() {
		List<Pizza> pizzas_ = this.pizzas;
		this.pizzas = new ArrayList<>();
		return pizzas_;
	}

	public void addIsogloss(List<Isogloss> isogloss) {
		this.isogloss.addAll(isogloss);
	}

	protected boolean hasIsogloss() {
		return !isogloss.isEmpty();
	}

	protected List<Isogloss> getIsogloss() {
		List<Isogloss> iso_ = isogloss;
		isogloss = new ArrayList<>();
		return iso_;
	}

	public void setGradient(Gradient g) {
		gradient = g;
	}

	protected boolean hasGradient() {
		return gradient != null;
	}

	protected Gradient getGradient() {
		Gradient g = gradient;
		gradient = null;
		return g;
	}

	public void setLabels(String[] l, Color[] c) throws Exception {
		if (l == null || c == null || l.length != c.length) {
			throw new Exception("Error setting label.");
		}
		labels = l;
		colors = c;
	}

	public void setLabels(String[] l, Color[] c, int[] histogram, int[] colorIndexes) throws Exception {
		if (l == null || c == null || l.length != c.length) {
			throw new Exception("Error setting label.");
		}
		labels = l;
		colors = c;
		integers = histogram;
		cIdx = colorIndexes;
	}

	public void setLabels(String[] l, Color[] c, float[] histogram, int[] colorIndexes) throws Exception {
		if (l == null || c == null || l.length != c.length) {
			throw new Exception("Error setting label.");
		}
		labels = l;
		colors = c;
		floats = histogram;
		cIdx = colorIndexes;
	}

	protected boolean hasLabel() {
		return labels != null && colors != null;
	}

	public String[] getLabels() {
		String[] l = labels;
		labels = null;
		return l;
	}

	public Color[] getColors() {
		Color[] c = colors;
		colors = null;
		return c;
	}

	public float[] getFloats() {
		float[] c = floats;
		floats = null;
		return c;
	}

	public int[] getIntegers() {
		int[] c = integers;
		integers = null;
		return c;
	}

	public int[] getColorIndexes() {
		int[] c = cIdx;
		cIdx = null;
		return c;
	}
}

class OpenGLEngine {

	private static final int FPS = 60;

	public static String readFile(InputStream data, Charset encoding) {
		String value = "";
		Scanner in = new Scanner(data);
		while (in.hasNext()) {
			value += in.nextLine() + "\n";
		}
		return value;
	}

	protected static int nextPo2(int x) {
		int npot = 1;
		while (npot < x) {
			npot *= 2;
		}
		return npot;
	}

	private Texture mapTexture = null;
	private Texture UI = null;
	private boolean stop = false;
	private final float wT_2, hT_2;
	private final float s, t;
	private float xI = 0, yI = 0;
	private float angleX = 0, angleY = 0, angleZ = 0;
	private float zoom = 1;
	private List<Pizza> pizzas = null;
	private OpenGLPanel panel;
	private boolean mouseLeftPressed = false;
	private boolean mouseMovedSincePress = false;
	private final DisplayMode mode;
	private List<Isogloss> isogloss = null;
	private Gradient gradient = null;
	private boolean drawMenu = false;
	private Labels labels = null;

	OpenGLEngine(BufferedImage map, OpenGLPanel panel, boolean fullscreen) throws LWJGLException, IOException, Exception {
		if (Display.isCreated()) {
			throw new Exception("Only one display can be active at any given moment.");
		}
		this.panel = panel;
		mode = Display.getDesktopDisplayMode();
		Display.setDisplayMode(mode);
		Display.setFullscreen(true);
		Display.create();
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, mode.getWidth(), mode.getHeight(), 0, 5000, -5000);
		GL11.glClearColor(1, 1, 1, 1);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		if (map != null) {
			if (mapTexture != null) {
				mapTexture.release();
			}
			ByteArrayInputStream bais;
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream(map.getWidth() * map.getHeight() * 4)) {
				ImageIO.write(map, "PNG", baos);
				bais = new ByteArrayInputStream(baos.toByteArray());
			}
			mapTexture = TextureLoader.getTexture("PNG", bais);
			bais.close();
			int w = map.getWidth();
			int h = map.getHeight();
			wT_2 = (float) w / 2.f;
			hT_2 = (float) h / 2.f;
			s = (float) w / (float) nextPo2(w);
			t = (float) h / (float) nextPo2(h);
		} else {
			wT_2 = 0;
			hT_2 = 0;
			s = 0;
			t = 0;
		}
		if (UI == null) {
			UI = TextureLoader.getTexture("PNG", OpenGLEngine.class.getResourceAsStream("/Resources/UI.png"));
		}
	}

	void stop() {
		stop = true;
	}

	void start() throws Exception {
		stop = false;
		while (!stop) {
			if (Display.isCloseRequested()) {
				stop = true;
				break;
			}
			if (panel.hasPizzas()) {
				if (pizzas == null) {
					pizzas = new ArrayList<>();
				}
				pizzas.addAll(panel.getPizzas());
				adjustPizzaRadius();
			}
			if (panel.hasIsogloss()) {
				if (isogloss == null) {
					isogloss = new ArrayList<>();
				}
				isogloss.addAll(panel.getIsogloss());
			}
			if (panel.hasGradient()) {
				gradient = panel.getGradient();
			}
			if (panel.hasLabel()) {
				float[] fs = panel.getFloats();
				int[] is = panel.getIntegers();
				if (fs != null) {
					labels = new Labels(panel.getLabels(), panel.getColors(), fs, panel.getColorIndexes(), gradient, mode.getWidth() - 300, 0, 300, mode.getHeight());
				} else if (is != null) {
					labels = new Labels(panel.getLabels(), panel.getColors(), is, panel.getColorIndexes(), gradient, mode.getWidth() - 300, 0, 300, mode.getHeight());
				} else {
					labels = new Labels(panel.getLabels(), panel.getColors(), gradient, mode.getWidth() - 300, 0, 300, mode.getHeight());
				}
			}
			processInput();
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glClearColor(1, 1, 1, 1);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glColor3f(1, 1, 1);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, mapTexture.getTextureID());
			GL11.glShadeModel(GL11.GL_SMOOTH);
			GL11.glLoadIdentity();
			GL11.glTranslatef(xI + wT_2, yI + hT_2, 0);
			GL11.glRotatef(angleX, 1, 0, 0);
			GL11.glRotatef(angleY, 0, 1, 0);
			GL11.glRotatef(angleZ, 0, 0, 1);
			GL11.glScalef(zoom, zoom, zoom);
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(0, t);
			GL11.glVertex2f(-wT_2, hT_2);
			GL11.glTexCoord2f(s, t);
			GL11.glVertex2f(wT_2, hT_2);
			GL11.glTexCoord2f(s, 0);
			GL11.glVertex2f(wT_2, -hT_2);
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(-wT_2, -hT_2);
			GL11.glEnd();
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
			if (pizzas != null) {
				for (Pizza pizza : pizzas) {
					pizza.draw();
				}
			}
			if (isogloss != null) {
				for (Isogloss iso : isogloss) {
					iso.draw();
				}
			}
			if (gradient != null) {
				gradient.draw();
			}
			if (labels != null) {
				GL11.glPushMatrix();
				GL11.glLoadIdentity();
				GL11.glScissor(mode.getWidth() - 300, 0, 300, mode.getHeight());
				GL11.glEnable(GL11.GL_SCISSOR_TEST);
				GL11.glClearColor(0.9f, 0.9f, 0.9f, 1);
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
				labels.draw();
				GL11.glDisable(GL11.GL_SCISSOR_TEST);
				GL11.glPopMatrix();
			}
			drawMenu();
			Display.update();
			Display.sync(FPS);
		}
		if (stop) {
			if (mapTexture != null) {
				mapTexture.release();
				mapTexture = null;
			}
			if (pizzas != null) {
				pizzas.stream().forEach((pizza) -> pizza.destroy());
			}
			if (isogloss != null) {
				isogloss.stream().forEach((iso) -> iso.destroy());
			}
			if (gradient != null) {
				gradient.destroy();
			}
			if (labels != null) {
				labels.destroy();
			}
			Display.destroy();
		}
	}

	private void processInput() throws Exception {
		processKeyboardInput();
		proccessMouseInput();
	}

	private void proccessMouseInput() throws Exception {
		int mouseX = Mouse.getDX();
		int mouseY = Mouse.getDY();
		final int MOUSE_LEFT = 0, MOUSE_RIGHT = 1, MOUSE_MIDDLE = 2;
		if (mouseLeftPressed && !Mouse.isButtonDown(MOUSE_LEFT)) {
			mouseLeftPressed = false;
			if (mouseX == 0 && mouseY == 0 && !mouseMovedSincePress) {
				detectUIClicks();
				if (pizzas != null && !pizzas.isEmpty()) {
					detectPizza();
				}
				if (isogloss != null && !isogloss.isEmpty()) {
					detectIsogloss();
				}
				if (gradient != null) {
					detectGradient();
				}
			}
			mouseMovedSincePress = false;
		}
		if (Mouse.isButtonDown(MOUSE_LEFT) && !Mouse.isButtonDown(MOUSE_RIGHT)) {
			xI += mouseX;
			yI -= mouseY;
			mouseLeftPressed = true;
			if (mouseX != 0 && mouseY != 0) {
				mouseMovedSincePress = true;
			}
		}
		if (Mouse.isButtonDown(MOUSE_RIGHT) && !Mouse.isButtonDown(MOUSE_LEFT)) {
			angleY -= mouseX;
			angleX -= mouseY;
			if (angleX < -45) {
				angleX = -45;
			}
			if (angleX > 45) {
				angleX = 45;
			}
			if (angleY > 45) {
				angleY = 45;
			}
			if (angleY < -45) {
				angleY = -45;
			}
		}
		if (Mouse.isButtonDown(MOUSE_LEFT) && Mouse.isButtonDown(MOUSE_RIGHT) || Mouse.isButtonDown(MOUSE_MIDDLE)) {
			angleZ += mouseX;
			zoom += 0.05f * mouseY;
			if (Mouse.hasWheel()) {
				zoom += 0.05f * Mouse.getDWheel();
			}
			if (zoom > 10) {
				zoom = 10;
			}
			if (zoom < 0.05f) {
				zoom = 0.05f;
			}
		}
		if (Mouse.hasWheel()) {
			int w = Mouse.getDWheel();
			zoom += 0.05f * (w > 0 ? 1 : w < 0 ? -1 : 0);
			if (zoom > 10) {
				zoom = 10;
			}
			if (zoom < 0.05f) {
				zoom = 0.05f;
			}
		}
		drawMenu = Mouse.getX() <= 90 && (mode.getHeight() - Mouse.getY()) <= 210;
	}

	private void processKeyboardInput() {
		if (Keyboard.isKeyDown(Keyboard.KEY_RCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
			if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
				angleX -= 1;
				if (angleX < -45) {
					angleX = -45;
				}
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				angleX += 1;
				if (angleX > 45) {
					angleX = 45;
				}
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				angleY += 1;
				if (angleY > 45) {
					angleY = 45;
				}
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
				angleY -= 1;
				if (angleY < -45) {
					angleY = -45;
				}
			}
		} else if (Keyboard.isKeyDown(Keyboard.KEY_RSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				angleZ -= 1;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
				angleZ += 1;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				zoom += 0.05f;
				if (zoom > 10) {
					zoom = 10;
				}
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
				zoom -= 0.05f;
				if (zoom < 0.05f) {
					zoom = 0.05f;
				}
			}
		} else {
			if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				xI -= zoom;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
				xI += zoom;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				yI += zoom;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
				yI -= zoom;
			}
		}
	}

	private void detectPizza() throws Exception {
		GL11.glDisable(GL11.GL_DITHER);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		pizzas.stream().forEach((p) -> {
			try {
				p.draw();
			} catch (Exception ex) {
			}
		});
		IntBuffer viewport = BufferUtils.createIntBuffer(16);
		FloatBuffer modelView = BufferUtils.createFloatBuffer(16);
		FloatBuffer projection = BufferUtils.createFloatBuffer(16);
		FloatBuffer z = BufferUtils.createFloatBuffer(1);
		FloatBuffer position = BufferUtils.createFloatBuffer(3);
		GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, modelView);
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, projection);
		GL11.glGetInteger(GL11.GL_VIEWPORT, viewport);
		GL11.glReadPixels(Mouse.getX(), Mouse.getY(), 1, 1, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, z);
		GLU.gluUnProject(Mouse.getX(), Mouse.getY(), z.get(0), modelView, projection, viewport, position);
		float x_ = position.get(0);
		float y_ = position.get(1);
		pizzas.stream().forEach((p) -> p.toggleSelected(x_, y_));
		adjustSelectedPizzas();
	}

	private void detectIsogloss() throws Exception {
		int noIsogloss = isogloss.size();
		byte dc = (byte) (256 / noIsogloss);
		Byte[] colors = new Byte[noIsogloss];
		for (byte i = 0; i < noIsogloss; i++) {
			colors[i] = (byte) (i * dc);
		}
		GL11.glDisable(GL11.GL_DITHER);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		for (int i = 0; i < noIsogloss; i++) {
			isogloss.get(i).draw(colors[i]);
		}
		GL11.glEnable(GL11.GL_DITHER);
		ByteBuffer pixel = ByteBuffer.allocateDirect(3);
		GL11.glReadPixels(Mouse.getX(), Mouse.getY(), 1, 1, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, pixel);
		byte detected = pixel.get(0);
		for (int i = 0; i < noIsogloss; i++) {
			if (colors[i] == detected) {
				isogloss.get(i).toggleSelected();
				break;
			}
		}
	}

	private void detectGradient() throws Exception {
		gradient.detectSelection(Mouse.getX(), Mouse.getY());
	}

	private void adjustPizzaRadius() {
		final float maxPossibleRadius = 200;
		float minDist = Float.POSITIVE_INFINITY;
		for (Pizza pizza : pizzas) {
			for (Pizza pizza2 : pizzas) {
				if (pizza != pizza2) {
					float dist = (float) new Point2D.Float(pizza.getXC(), pizza.getYC()).distance(pizza2.getXC(), pizza2.getYC());
					if (dist < minDist) {
						minDist = dist;
					}
				}
			}
		}
		minDist *= 2.f / 5.f;
		if (minDist > maxPossibleRadius) {
			minDist = maxPossibleRadius;
		}
		Pizza.setPizzaRadius(minDist);
		pizzas.stream().forEach((pizza) -> pizza.resetSize());
	}

	private void adjustSelectedPizzas() {
		List<Pizza> selPs = new ArrayList<>();
		pizzas.stream().filter((pizza) -> pizza.isSelected()).forEach((pizza) -> selPs.add(pizza));
		if (selPs.isEmpty()) {
			return;
		}
		if (selPs.size() == 1) {
			selPs.get(0).setXYSel(selPs.get(0).getXC(), selPs.get(0).getYC());
			return;
		}
		float xM = 0, yM = 0;
		for (Pizza p : selPs) {
			xM += p.getXC();
			yM += p.getYC();
		}
		xM /= selPs.size();
		yM /= selPs.size();
		float xMF = xM;
		float yMF = yM;
		selPs.sort((Pizza o1, Pizza o2) -> {
			double angle1 = Math.atan2(o1.getYC() - yMF, o1.getXC() - xMF);
			double angle2 = Math.atan2(o2.getYC() - yMF, o2.getXC() - xMF);
			if (angle1 < 0) {
				angle1 += Math.PI * 2;
			}
			if (angle2 < 0) {
				angle2 += Math.PI * 2;
			}
			angle1 *= 100;
			angle2 *= 100;
			return (int) Math.round(angle1 - angle2);
		});
		float radius = Pizza.getPizzaRadius() * 2;
		float angle = 0;
		float angleStep = (float) (2 * Math.PI / selPs.size());
		for (Pizza p : selPs) {
			float x = (float) (xM + Math.cos(angle) * radius);
			float y = (float) (yM + Math.sin(angle) * radius);
			angle += angleStep;
			p.setXYSel(x, y);
		}
	}

	private void detectUIClicks() {
		int x = Mouse.getX();
		int y = mode.getHeight() - Mouse.getY();
		if (y < 15) {
			// nothing
		} else if (y <= 33) {
			if (x >= 33 && x <= 53) {
				yI += 5;
			}
		} else if (y <= 52) {
			if (x < 13) {
				// nothing
			} else if (x <= 33) {
				xI -= 5;
			} else if (x <= 53) {
				xI = 0;
				yI = 0;
				zoom = 1;
				angleX = 0;
				angleY = 0;
				angleZ = 0;
			} else if (x <= 73) {
				xI += 5;
			}
		} else if (y <= 69) {
			if (x >= 33 && x <= 53) {
				yI -= 5;
			}
		} else if (y <= 75) {
			// nothing
		} else if (y <= 109) {
			if (x < 11) {
				// nothing
			} else if (x <= 40) {
				zoom -= 0.05f;
			} else if (x <= 45) {
				// nothing
			} else if (x <= 74) {
				zoom += 0.05f;
			}
		} else if (y <= 119) {
			// nothing
		} else if (y <= 147) {
			if (x < 11) {
				// nothing
			} else if (x <= 40) {
				angleZ += 5;
			} else if (x <= 45) {
				// nothing
			} else if (x <= 74) {
				angleZ -= 5;
			}
		} else if (y <= 155) {
			// nothing
		} else if (y <= 188) {
			if (x < 11) {
				// nothing
			} else if (x <= 40) {
				// TODO camera
			} else if (x <= 45) {
				// nothing
			} else if (x <= 74) {
				stop = true;
			}
		}
	}

	private void drawMenu() {
		if (!drawMenu) {
			return;
		}
		float uit = 200.f / 256.f;
		float uis = 75.f / 128.f;
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glColor3f(1, 1, 1);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, UI.getTextureID());
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0);
		GL11.glVertex3f(5, 5, 0);
		GL11.glTexCoord2f(uis, 0);
		GL11.glVertex3f(80, 5, 0);
		GL11.glTexCoord2f(uis, uit);
		GL11.glVertex3f(80, 200, 0);
		GL11.glTexCoord2f(0, uit);
		GL11.glVertex3f(5, 200, 0);
		GL11.glEnd();
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		GL11.glPopMatrix();
	}

	private long getTime() {
		return Sys.getTime() * 1000 / Sys.getTimerResolution();
	}
}
