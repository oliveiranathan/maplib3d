/*
 Copyright (c) 2014 Nathan Oliveira<oliveiranathan (at) gmail (dot) com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
package OpenGLPanel;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentHashMap;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Gradient {

	private static final float maxZValue = 500;
	private static final int step = 5;
	private static int vertShadID = 0;
	private static final String vertexShaderCode = OpenGLEngine.readFile(Gradient.class.getResourceAsStream("/OpenGLPanel/gradient.vert"), Charset.defaultCharset());
	private static int fragShadID = 0;
	private static final String fragmentShaderCode = OpenGLEngine.readFile(Gradient.class.getResourceAsStream("/OpenGLPanel/gradient.frag"), Charset.defaultCharset());
	private static int shader = 0;
	private static int pointerScale = 0;
	private static int pointerMax = 0;
	private static int pointerProp = 0;
	private static int pointerMask = 0;
	private static int pointerSize = 0;
	private static int pointerRedBand = 0;
	private static Texture textureMask = null;

	private final ConcurrentHashMap<Point2D.Double, Integer> pointsMap;
	private final int w, h;
	private final int cols, rows;
	private final int max;
	private final float scale;
	private float[] vertices = null;
	private int redBand = -1;

	private final boolean[][] mask;
	private BufferedImage maskImg = null;
	private final float s, t;

	public Gradient(ConcurrentHashMap<Point2D.Double, Integer> pointsMap, boolean[][] mask, int w, int h) {
		this.pointsMap = pointsMap;
		this.w = w;
		this.h = h;
		int m = 0;
		for (int i : pointsMap.values()) {
			if (i > m) {
				m = i;
			}
		}
		max = m;
		scale = maxZValue / max;
		cols = w / step + 1;
		rows = h / step + 1;
		this.mask = mask;
		s = (float) w / OpenGLEngine.nextPo2(w);
		t = (float) h / OpenGLEngine.nextPo2(h);
	}

	public Gradient(ConcurrentHashMap<Point2D.Double, Integer> pointsMap, BufferedImage mask, int w, int h) {
		this.pointsMap = pointsMap;
		this.w = w;
		this.h = h;
		int m = 0;
		for (int i : pointsMap.values()) {
			if (i > m) {
				m = i;
			}
		}
		max = m;
		scale = maxZValue / max;
		cols = w / step + 1;
		rows = h / step + 1;
		this.mask = null;
		maskImg = mask;
		s = (float) w / OpenGLEngine.nextPo2(w);
		t = (float) h / OpenGLEngine.nextPo2(h);
	}

	protected void draw() throws Exception {
		createVerticesAndShaders();
		GL11.glPushMatrix();
		GL11.glTranslatef(-w / 2, -h / 2, 0);
		ARBShaderObjects.glUseProgramObjectARB(shader);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureMask.getTextureID());
		ARBShaderObjects.glUniform1fARB(pointerScale, scale);
		ARBShaderObjects.glUniform1fARB(pointerMax, max);
		ARBShaderObjects.glUniform1iARB(pointerMask, 0);
		ARBShaderObjects.glUniform1iARB(pointerRedBand, redBand);
		ARBShaderObjects.glUniform2fARB(pointerSize, w, h);
		ARBShaderObjects.glUniform2fARB(pointerProp, s, t);
		for (int i = 0; i < cols - 1; i++) {
			GL11.glBegin(GL11.GL_TRIANGLE_STRIP);
			for (int j = 0; j < rows; j++) {
				GL11.glVertex3f(i * step, j * step, vertices[rows * i + j]);
				GL11.glVertex3f((i + 1) * step, j * step, vertices[rows * (i + 1) + j]);
			}
			GL11.glEnd();
		}
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
		ARBShaderObjects.glUseProgramObjectARB(0);
		GL11.glPopMatrix();
	}

	protected void destroy() {
		if (shader != 0) {
			ARBShaderObjects.glDeleteObjectARB(shader);
			shader = 0;
			pointerMax = 0;
			pointerScale = 0;
			pointerMask = 0;
			pointerProp = 0;
			pointerScale = 0;
			pointerRedBand = 0;
		}
		if (vertShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(vertShadID);
			vertShadID = 0;
		}
		if (fragShadID != 0) {
			ARBShaderObjects.glDeleteObjectARB(fragShadID);
			fragShadID = 0;
		}
		if (textureMask != null) {
			textureMask.release();
			textureMask = null;
		}
	}

	@SuppressWarnings("ValueOfIncrementOrDecrementUsed")
	private void createVerticesAndShaders() throws Exception {
		if (textureMask == null) {
			if (mask != null) {
				maskImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
				for (int x = 0; x < w; x++) {
					for (int y = 0; y < h; y++) {
						if (mask[x][y]) {
							maskImg.setRGB(x, y, Color.WHITE.getRGB());
						}
					}
				}
			}
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream(w * h * 4)) {
				ImageIO.write(maskImg, "PNG", baos);
				try (ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray())) {
					textureMask = TextureLoader.getTexture("PNG", bais);
				}
			} catch (IOException ex) {
				textureMask = null;
			}
		}
		if (vertices == null) {
			vertices = new float[cols * rows];
			int idx = 0;
			for (int x = 0; x <= w; x += step) {
				for (int y = 0; y <= h; y += step) {
					double dTotal = 0;
					double result = 0;
					for (Point2D.Double p : pointsMap.keySet()) {
						double d = Math.pow(p.distance(x, y), 4);
						if (d > 0) {
							d = 1.0 / d;
						} else {
							d = 1.e20;
						}
						result += pointsMap.get(p) * d;
						dTotal += d;
					}
					if (dTotal > 0) {
						vertices[idx++] = (float) (-scale * result / dTotal);
					} else {
						vertices[idx++] = 0;
					}
				}
			}
		}
		if (vertShadID == 0) {
			vertShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
			if (vertShadID == 0) {
				throw new Exception("Cannot create the vertex shader");
			}
			ARBShaderObjects.glShaderSourceARB(vertShadID, vertexShaderCode);
			ARBShaderObjects.glCompileShaderARB(vertShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(vertShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(vertShadID);
				vertShadID = 0;
				throw new Exception("Cannot compile vertex shader.");
			}
		}
		if (fragShadID == 0) {
			fragShadID = ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
			if (fragShadID == 0) {
				throw new Exception("Cannot create the fragment shader.");
			}
			ARBShaderObjects.glShaderSourceARB(fragShadID, fragmentShaderCode);
			ARBShaderObjects.glCompileShaderARB(fragShadID);
			if (ARBShaderObjects.glGetObjectParameteriARB(fragShadID, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE) {
				ARBShaderObjects.glDeleteObjectARB(fragShadID);
				fragShadID = 0;
				throw new Exception("Cannot compile fragment shader.");
			}
		}
		if (shader == 0) {
			shader = ARBShaderObjects.glCreateProgramObjectARB();
			if (shader == 0) {
				throw new Exception("Cannot create the shader program.");
			}
			ARBShaderObjects.glAttachObjectARB(shader, vertShadID);
			ARBShaderObjects.glAttachObjectARB(shader, fragShadID);
			ARBShaderObjects.glLinkProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error linking the shader.");
			}
			ARBShaderObjects.glValidateProgramARB(shader);
			if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE) {
				shader = 0;
				throw new Exception("Error validating the shader.");
			}
			pointerScale = ARBShaderObjects.glGetUniformLocationARB(shader, "scale");
			pointerMax = ARBShaderObjects.glGetUniformLocationARB(shader, "max");
			pointerMask = ARBShaderObjects.glGetUniformLocationARB(shader, "mask");
			pointerProp = ARBShaderObjects.glGetUniformLocationARB(shader, "prop");
			pointerSize = ARBShaderObjects.glGetUniformLocationARB(shader, "size");
			pointerRedBand = ARBShaderObjects.glGetUniformLocationARB(shader, "redBand");
		}
	}

	protected void detectSelection(int x, int y) throws Exception {
		GL11.glDisable(GL11.GL_DITHER);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		draw();
		GL11.glEnable(GL11.GL_DITHER);
		IntBuffer viewport = BufferUtils.createIntBuffer(16);
		FloatBuffer modelView = BufferUtils.createFloatBuffer(16);
		FloatBuffer projection = BufferUtils.createFloatBuffer(16);
		FloatBuffer z = BufferUtils.createFloatBuffer(1);
		FloatBuffer position = BufferUtils.createFloatBuffer(3);
		GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, modelView);
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, projection);
		GL11.glGetInteger(GL11.GL_VIEWPORT, viewport);
		GL11.glReadPixels(x, y, 1, 1, GL11.GL_DEPTH_COMPONENT, GL11.GL_FLOAT, z);
		GLU.gluUnProject(x, y, z.get(0), modelView, projection, viewport, position);
		float x_ = position.get(0) + maskImg.getWidth() / 2;
		float y_ = position.get(1) + maskImg.getHeight() / 2;
		float z_ = -position.get(2);
		int oldRB = redBand;
		redBand = -1;
		if (z_ >= 0 && z_ <= maxZValue && maskImg.getRGB((int) x_, (int) y_) == Color.WHITE.getRGB()) {
			redBand = Math.round(z_ / (maxZValue / max));
		}
		if (redBand != -1 && oldRB == redBand) {
			redBand = -1;
		}
	}

	public int getRedBand() {
		return redBand;
	}
}
